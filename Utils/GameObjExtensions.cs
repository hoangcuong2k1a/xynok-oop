﻿using UnityEngine;

namespace Zk1Core.Utils
{
    public static class GameObjExtensions
    {
        public static bool IsLayer(this GameObject gameObject, LayerMask layerMask)
        {
            if (gameObject == null)
            {
                Debug.LogError("gameObj is not valid !");
                return false;
            }
            
            return layerMask == (layerMask | (1 << gameObject.layer));
        }
     
}
}