﻿namespace Xynok.Utils
{
    public class AssetPath
    {
        public const string DONTDESTROY = "--- DONT DESTROY ---";
        // icons ...
        public const string ICONS = "Images/Items/";
        public const string ICON_EQUIPMENTS = "Images/Items/Equipments/";
        public const string ICON_WEAPONS = "Images/Items/Equipments/Weapons/";
        
        // player
        public const string PLAYER_MOTIONS = "Motions/Player/";
        
        
        // default motion's names
        public const string MOTION_ATTACK_01 = "Motions/Player/";
        public const string MOTION_ATTACK_02 = "Motions/Player/";
        public const string MOTION_ATTACK_CRITICAL = "Motions/Player/";
        public const string MOTION_ATTACK_SPECIAL = "Motions/Player/";
    
        // CURRENT_GAME
        public const string MODULES = "Modules/";
        public const string SLOTS = "Slots/";
        
    }
}