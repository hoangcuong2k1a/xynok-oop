﻿using System;
using Xynok.Patterns.Singleton;

namespace Xynok.Utils
{
    public class TimeCycle : ALazySingleton<TimeCycle>
    {
        private Action _onUpdate;
        private Action _onFixedUpdate;
        

        private void Update()
        {
            _onUpdate?.Invoke();
        }

        private void FixedUpdate()
        {
            _onFixedUpdate?.Invoke();
        }

        public void AddListenerOnFixedUpdate(Action action)
        {
            _onFixedUpdate -= action;
            _onFixedUpdate += action;
        }

        public void RemoveListenerOnFixedUpdate(Action action)
        {
            _onFixedUpdate -= action;
        }

        public void RemoveAllListenerOnFixedUpdate()
        {
            _onFixedUpdate = default;
        }

        public void AddListenerOnUpdate(Action action)
        {
            _onUpdate -= action;
            _onUpdate += action;
        }

        public void RemoveListenerOnUpdate(Action action)
        {
            _onUpdate -= action;
        }

        public void RemoveAllListenerOnUpdate()
        {
            _onUpdate = default;
        }
    }
}