﻿using UnityEngine;

namespace Zk1Core.Utils
{
    public static class TransformExtensions
    {
        public static void ClearAllChild(this Transform parent)
        {
            foreach (Transform child in parent)
            {
                Object.Destroy(child.gameObject);
            }
        }
    }
}