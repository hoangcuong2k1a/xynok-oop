﻿#if UNITY_EDITOR

using System.IO;
using UnityEditor;

namespace Xynok.Utils
{
    public struct FileUtils
    {
       public static void DeleteAllExistedFile(string path)
        {
            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                if (path.Contains("Assets")) AssetDatabase.DeleteAsset(file);
                else File.Delete(file);
            }

            if (path.Contains("Assets")) AssetDatabase.SaveAssets();
        }
    }
}
#endif
