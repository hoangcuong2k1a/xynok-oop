﻿using UnityEngine;

namespace Xynok.Utils
{
    public static class ColorUtils
    {
        public static Color32 GREEN = new(71, 255, 65, 255);
        public static Color32 RED = new(239, 64, 24, 255);
        public static Color32 WHITE = new(255, 255, 255, 255);
    }
}