﻿using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Xynok.Utils
{
    public struct MathUtils
    {
        public struct Distance 
        {
            /// <summary>
            /// Returns the distance from pointA to line made by pointB and pointC
            /// </summary>
            public static float DistanceFromPointToLine(Vector2 pointA, Vector2 pointB, Vector2 pointC)
            {
                Vector2 v = pointB - pointA;
                Vector2 w = pointC - pointA;
                return math.sqrt(Vector2.Dot(v, v) + Vector2.Dot(w, w));
            }

            /// <summary>
            /// Returns the distance from pointA to line made by pointB and pointC
            /// </summary>
            public static float DistanceFromPointToLine(Vector3 pointA, Vector3 pointB, Vector3 pointC)
            {
                Vector3 v = pointB - pointA;
                Vector3 w = pointC - pointA;
                return math.sqrt(Vector3.Dot(v, v) + Vector3.Dot(w, w));
            }

            /// <summary>
            /// Calculates the point on line made by pointA and pointB that is closest to pointC
            /// </summary>
            public static Vector2 GetClosetPointOnLine(Vector2 pointC, Vector2 pointA, Vector2 pointB,
                bool forceBetweenAandB = false)
            {
                Vector2 v = pointB - pointA;
                Vector2 w = pointC - pointA;
                float t = Vector2.Dot(w, v) / Vector2.Dot(v, v);
                if (forceBetweenAandB)
                {
                    t = math.clamp(t, 0, 1);
                }

                return pointA + t * v;
            }


            /// <summary>
            ///  Calculates the point on line made by pointA and pointB that is closest to pointC
            /// </summary>
            public static Vector3 GetClosetPointOnLine(Vector3 pointC, Vector3 pointA, Vector3 pointB,
                bool forceBetweenAandB = false)
            {
                Vector3 v = pointB - pointA;
                Vector3 w = pointC - pointA;
                float t = Vector3.Dot(w, v) / Vector3.Dot(v, v);
                if (forceBetweenAandB)
                {
                    t = math.clamp(t, 0, 1);
                }

                return pointA + t * v;
            }
        }
        
        public struct Compare
        {
            /// <summary>
            ///  Returns the closet point from pointA to points
            /// </summary>
            public static Vector3 GetClosetPoint(Vector3 pointA, Vector3[] points)
            {
                Vector3 closestPoint = Vector3.zero;
                float closestDistance = float.MaxValue;
                foreach (var point in points)
                {
                    float distance = math.distancesq(pointA, point); 
                    
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestPoint = point;
                    }
                }

                return closestPoint;
            }
        }


        /// <summary>
        /// Get a random point between pointA and pointB
        /// </summary>
        public static Vector3 GetRandomPointBetween(Vector3 pointA, Vector3 pointB)
        {
            return Vector3.Lerp(pointA, pointB, Random.value);
        }
    }
}