﻿namespace Xynok.Utils
{
    public class SymbolUtils
    {
        public const string Heart = "♥";
        public const string HeartEmpty = "♡";
        public const string RectFill = "█";
        public const string RectDot = "░";
        public const string SquareFill = "■";
        public const string SquareEmpty = "□";
    }
}