﻿using UnityEngine;

namespace Xynok.Utils
{
    public static class MatrixUtils
    {
        public static T[] GetRow<T>(T[,] matrix, int row)
        {
            if (row > matrix.GetLength(1) - 1)
            {
                Debug.LogError("Row index out of range");
                return null;
            }

            var height = matrix.GetLength(0);
            var rowVector = new T[height];
            for (var i = 0; i < height; i++)
            {
                rowVector[i] = matrix[i, row];
            }

            return rowVector;
        }
        
        public static T[] GetColumn<T>(T[,] matrix, int column)
        {
            if (column > matrix.GetLength(0) - 1)
            {
                Debug.LogError("Column index out of range");
                return null;
            }

            var width = matrix.GetLength(1);
            var colVector = new T[width];
            for (var i = 0; i < width; i++)
            {
                colVector[i] = matrix[column, i];
            }

            return colVector;
        }
    }
}