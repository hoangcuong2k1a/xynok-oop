﻿using System;
using UnityEngine;
using Zk1Core.Utils;

namespace Xynok.Utils
{
    public class ActionDuration : IDisposable
    {
        private Action _action;
        private float _t;
        private float _duration;
        private bool _done;


        public void Start(Action action, float duration)
        {
            if (!_done && _action != default) Dispose();

            _duration = duration;
            _action = action;
            _t = _duration;
            _done = false;

            TimeCycle.Instance.AddListenerOnUpdate(Run);
        }

        void Run()
        {
            if (_duration == 0)
            {
                _action?.Invoke();
                Dispose();
                return;
            }

            if (_t > 0) _t -= Time.deltaTime;
            else
            {
                _action?.Invoke();
                Dispose();
            }
        }

        public void Dispose()
        {
            _done = true;
            if (TimeCycle.Instance) TimeCycle.Instance.RemoveListenerOnUpdate(Run);
        }
    }
}