﻿using UnityEngine.EventSystems;

namespace Xynok.Utils
{
    public class GUIUtils
    {
        public static bool ClickOnGUI()
        {
            return EventSystem.current.IsPointerOverGameObject();
        }
    }
}