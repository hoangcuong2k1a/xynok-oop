using System;

namespace Xynok.APIs
{
    /// <summary>
    /// implementation of DI (dependency injection)
    /// </summary>
    /// <typeparam name="T">dependency</typeparam>
    public interface IInjectable<in T> : IDisposable
    {
        void SetDependency(T dependency);
    }
    
}