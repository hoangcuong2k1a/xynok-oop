﻿using System;

namespace Xynok.APIs
{
    public interface IBinder<out T>
    {
        void AddListener(Action<T> listener);
        void RemoveListener(Action<T> listener);
    }
}