using UnityEngine;

namespace Xynok.Patterns.Singleton
{
    public abstract class ASingleton<T> : MonoBehaviour
    {
        public static T Instance;

        protected virtual void Awake()
        {
            Instance ??= gameObject.GetComponent<T>();
        }
    }
}