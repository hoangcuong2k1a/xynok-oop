using UnityEngine;

namespace Xynok.Patterns.Singleton
{
    public abstract class ALazySingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                _instance ??= FindObjectOfType<T>();
                if (_instance) return _instance;
                GameObject gameObject = new GameObject($"singleton_{typeof(T).Name}");
                _instance = gameObject.AddComponent<T>();

                return _instance;
            }
        }

    }
}