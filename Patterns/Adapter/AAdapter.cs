﻿
namespace Zk1Core.Patterns.Adapter
{
    /// <summary>
    /// convert obj A -> obj B
    /// </summary>
    public abstract class AAdapter<T1>
    {
        protected T1 Source;

        protected AAdapter(T1 source)
        {
            Source = source;
        }
        
        
    }
}