using System;
using System.Collections.Generic;
using UnityEngine;
using Xynok.APIs;
using Xynok.Patterns.Factory.Reflection;

namespace ALCCore.Patterns.Factory.Reflection
{
    public abstract class ATreeFactory<T0, T1, T2> : AFactory<T0, T1>, IInjectable<T2>
        where T0 : Enum
        where T1 : ITypeOnFactory<T0>, IInjectable<T2>
    {
        private T2 _root;
        private Dictionary<T0, T1> _branches;
        private Action _onDispose;

        public virtual void SetDependency(T2 dependency)
        {
            if (_root != null) Dispose();
            _branches ??= new();
            _root = dependency;
        }

        public void CreateBranch(T0 id)
        {
            if (_branches.ContainsKey(id))
            {
                Debug.LogError($"Duplicated ! {_root.GetType().Name} had existed {id}.");
                return;
            }

            T1 branch = GetProduct(id);
            branch.SetDependency(_root);

            _onDispose += branch.Dispose;
            _branches.Add(id, branch);
        }

        public void RemoveBranch(T0 id)
        {
            if (!_branches.ContainsKey(id))
            {
                Debug.LogError($"{_root.GetType().Name} has not existed {id}.");
                return;
            }

            _onDispose -= _branches[id].Dispose;

            _branches[id].Dispose();

            _branches.Remove(id);
        }

        public void Dispose()
        {
            _onDispose?.Invoke();
            _onDispose = default;
            _branches?.Clear();
        }
    }
}