using System;
using System.Collections.Generic;
using System.Linq;
using ALCCore.Patterns.Factory.Reflection;
using UnityEngine;

namespace Xynok.Patterns.Factory.Reflection
{
    public interface ITypeOnFactory<out T> where T : Enum
    {
        T GetProductType();
    }
    public abstract class AFactory<T1, T2> where T1 : Enum where T2 : ITypeOnFactory<T1>
    {
        public readonly Dictionary<T1, Type> ProductTypes;

        protected AFactory()
        {
            ProductTypes = new Dictionary<T1, Type>();
            Init();
        }

        void Init()
        {
            List<Type> allTypes = FactoryHelper.FindDerivedTypes(typeof(T2)).ToList();

            foreach (var type in allTypes)
            {
                T2 productTemp = (T2)Activator.CreateInstance(type);
                
                T1 typeIDOfProduct = productTemp.GetProductType();
                if (!ProductTypes.ContainsKey(typeIDOfProduct))
                {
                    ProductTypes.Add(typeIDOfProduct, type);
                }
            }
        }
 
        public  T2 GetProduct(T1 typeID)
        {
            if (!ProductTypes.ContainsKey(typeID))
            {
                Debug.LogError($"not existed implement has type's {typeID}");
                throw new();
            }

            return (T2)Activator.CreateInstance(ProductTypes[typeID]);
        }
    }
}