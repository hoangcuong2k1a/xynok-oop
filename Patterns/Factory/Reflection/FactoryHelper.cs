using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ALCCore.Patterns.Factory.Reflection
{
    public struct FactoryHelper
    {
        public static IEnumerable<Type> FindDerivedTypes(Type baseType)
        {
            Assembly assembly = Assembly.GetAssembly(baseType);
            return assembly.GetTypes()
                .Where(type =>
                    type != baseType && !type.IsAbstract && baseType.IsAssignableFrom(type));
        }
    }
}