﻿using UnityEngine;
using Xynok.OOP.Data.Core.Primitives;
using Xynok.OOP.Entities.Core;
using Xynok.OOP.Services.Abilities.APIs;
using Xynok.OOP.Services.Abilities.Modules;
using Zk1Core.Services.Abilities.APIs;

namespace Xynok.OOP.Entities
{
    public interface ICharacter : IMovable3D, IAbilityAnimator
    {
    }

    public abstract class ACharacter : AEntity, ICharacter
    {
        [SerializeField] private Animator animator;
        private Vector3Value _moveDirection;
        public Animator Animator => animator;

        public Vector3Value MoveDirection
        {
            get
            {
                _moveDirection ??= new(Vector3.zero);
                return _moveDirection;
            }
        }
    }
}