﻿using System;
using ALCCore.Entities.APIs;
using Xynok.APIs;
using Xynok.OOP.Data.Core.SubModules;

namespace Xynok.OOP.Entities.APIs
{
    public interface IEntity<T> where T : Enum
    {
        
    }
    public interface IEntity : IPosition, IAbilityOwner, IInjectable<ResourceActionable>
    {
        ResourceActionable Data { get; }
    }

  
}