﻿using UnityEngine;

namespace ALCCore.Entities.APIs
{
    public interface IPosition
    {
        Transform GlobalTransform { get; }
        Vector3 CurrentPosition { get; }
    }
}