﻿using Xynok.OOP.Data.Core.SubModules;

namespace ALCCore.Entities.APIs
{
    public interface IAbilityOwner
    {
        AbilityCollection Abilities { get; }
    }
}