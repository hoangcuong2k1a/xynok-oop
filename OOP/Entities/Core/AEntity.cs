﻿using ALCCore.Services.Abilities.Core;
using UnityEngine;
using Xynok.OOP.Data.Core.SubModules;
using Xynok.OOP.Entities.APIs;

namespace Xynok.OOP.Entities.Core
{
    // [SerializeField] private ResourceActionable resourceActionable;
    //
    // private void Start()
    // {
    //     SetDependency(resourceActionable);
    // }
    /// <summary>
    /// obj contains a bunch of modules
    /// </summary>
    public abstract class AEntity : MonoBehaviour, IEntity
    {
        // APIs
        public Transform GlobalTransform => gameObject.transform;
        public Vector3 CurrentPosition => GlobalTransform.position;
        public ResourceActionable Data => _data;

        public AbilityCollection Abilities => Data.abilities;

        // FIXED SCOPE
        private ResourceActionable _data;
        private AbilityController _abilityController;

        public void SetDependency(ResourceActionable dependency)
        {
            if (_data != null) Dispose();
            _data = dependency;
            InitAbilities();
            Init();
        }

        protected virtual void Init()
        {
        }


        void InitAbilities()
        {
            _abilityController ??= new();
            _abilityController.SetDependency(this);
        }


        public virtual void Dispose()
        {
            _abilityController?.Dispose();
        }

        private void OnDestroy()
        {
            Dispose();
        }

    }
}