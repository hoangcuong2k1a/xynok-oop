﻿using Core.Runtime.Data.Enums;
using Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using VeThanCore.Data.Enums;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Primitives;
using Xynok.OOP.GUI.Modals;
using Xynok.Patterns.Singleton;
using Zk1Core.Patterns.Singleton;
using Zk1Core.Runtime.Managers.GUI;
using Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal;

namespace Zk1Core.Managers.GUI
{
    public class GUIController : ASingleton<GUIController>
    {
        public Canvas canvas;
        public Camera mainCam;

        [Title("State", "", TitleAlignments.Centered)]
        public BoolValue activeGui;

        [Title("Paths", "", TitleAlignments.Centered)] [ReadOnly]
        public string pagePrefix = "GUI/Pages/";

        [ReadOnly] public string modalPrefix = "GUI/Modals/";

        [Title("Containers", "", TitleAlignments.Centered)]
        public PageContainer pageContainer;

        public ModalContainer modalContainer;

        [Title("Configs", "", TitleAlignments.Centered)]
        public PageDict pageDict;

        public ModalDict modalDict;
        public GUIFlowConfig flowConfig;

        [ReadOnly] public PageType currentPage;
        [ReadOnly] public ModalType currentModal;
        [ReadOnly] public GUIType onTop;

        

        [Button]
        public void LoadPage(PageType pageType)
        {
            if (!CanLoadPage(pageType)) return;
            pageContainer.Push($"{pagePrefix}{pageDict[pageType]}", flowConfig.pageAnimEnter, flowConfig.pageStacking);
            currentPage = pageType;
            onTop = GUIType.Page;
        }


        [Button]
        public async void LoadModal(ModalType modalType)
        {
            await UniTask.WaitUntil(() => !modalContainer.IsInTransition);
            if (!CanLoadModal(modalType)) return;
            await modalContainer.Push($"{modalPrefix}{modalDict[modalType]}", flowConfig.modalAnimEnter).Task;
            currentModal = modalType;
            onTop = GUIType.Modal;
        }

        bool CanLoadPage(PageType pageType)
        {
            bool duplicated = currentPage == pageType;
            bool existed = pageDict.ContainsKey(pageType);
            if (!existed) CatchError($"not exist page {pageType}");
            if (duplicated) Debug.LogError($"duplicated page {pageType}");
            return !duplicated && existed;
        }

        bool CanLoadModal(ModalType modalType)
        {
            bool duplicated = currentModal == modalType;
            if (duplicated && !flowConfig.modalStacking)
            {
                Debug.LogError("config does not allow stack modal !");
                return false;
            }

            bool existed = modalDict.ContainsKey(modalType);

            if (!existed)
            {
                CatchError($"not exist modal {modalType}");
            }

            return existed;
        }

        
        void CatchError(string err)
        {
            modalContainer.Push<ModalQuickAlert>($"{modalPrefix}{modalDict[ModalType.Alert]}", true,
                (e) =>
                {
                    string value = $" This feature is on update. \n <color=#FF0000>BUG:</color> {err} ";
                    e.SetAlert(value);
                });
        }

        public void ActiveGUI(bool value)
        {
            activeGui.Value = value;
        }
    }
}