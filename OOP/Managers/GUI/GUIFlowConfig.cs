﻿using System;
using Core.Runtime.Data.Enums;
using VeThanCore.Data.Enums;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Dictionary;
using Zk1Core.Runtime.Data.Core.Dictionary;

namespace Zk1Core.Runtime.Managers.GUI
{
    [Serializable]
    public class PageDict : SerializableDictionary<PageType, string>
    {
    }
    [Serializable]
    public class ModalDict : SerializableDictionary<ModalType, string>
    {
    }

    [Serializable]
    public class GUIFlowConfig
    {
        public bool pageStacking = false;
        public bool modalStacking = true;

        public bool pageAnimEnter = true;
        public bool pageAnimExit = true;

        public bool modalAnimEnter = true;
        public bool modalAnimExit = true;
    }
}