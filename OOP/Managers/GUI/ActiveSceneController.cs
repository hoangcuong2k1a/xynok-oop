﻿using UnityEngine;
using VeThanCore.Data.Enums;
using Xynok.Enums;

namespace Zk1Core.Managers.GUI
{
    public class ActiveSceneController : MonoBehaviour
    {
        public PageType targetPage;

        private void Start()
        {
            GUIController.Instance.mainCam ??= Camera.main;
            if (targetPage != PageType.None) GUIController.Instance.LoadPage(targetPage);
        }
    }
}