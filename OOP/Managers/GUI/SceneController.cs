﻿using System;
using ALCCore.Enums;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using Xynok.OOP.Data.Core.Primitives;
using Xynok.Patterns.Singleton;

namespace Xynok.OOP.Managers.GUI
{
    [Serializable]
    public class SceneActive : EnumValue<SceneType>
    {
        public SceneActive(SceneType value) : base(value)
        {
        }
    }

    public class SceneController : ASingleton<SceneController>
    {
        public SceneActive currentScene;

        [Button]
        public async void LoadScene(SceneType sceneType, Action onLoadDone)
        {
            if (!CanLoadScene(sceneType)) return;

            currentScene.Value = sceneType;
            await SceneManager.LoadSceneAsync((int)sceneType - 1);
            onLoadDone?.Invoke();
        }

        [Button]
        public async void ReloadCurrentScene(SceneType sceneType, Action onLoadDone)
        {
            await SceneManager.LoadSceneAsync((int)currentScene.Value - 1);
            onLoadDone?.Invoke();
        }

        bool CanLoadScene(SceneType sceneType)
        {
            if (currentScene.Value == sceneType)
            {
                Debug.LogError($"current Scene is the same: {sceneType}");
                return false;
            }

            return true;
        }
    }
}