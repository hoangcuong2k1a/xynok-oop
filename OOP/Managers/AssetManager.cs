﻿using System.Collections.Generic;
using UnityEngine;
using Xynok.Patterns.Singleton;

namespace Xynok.OOP.Managers
{
    public class AssetManager: ALazySingleton<AssetManager>
    {
        private Dictionary<int, GameObject> _prefabs = new();
        private Dictionary<int, Object> _assets = new();
       
        public T GetPrefab<T>(string address) where T : MonoBehaviour
        {
            int hash = Animator.StringToHash(address);

            if (_prefabs.ContainsKey(hash)) return _prefabs[hash].GetComponent<T>();


            var result = Resources.Load<T>(address);
            if (result == null)
            {
                Debug.LogError($"not exist prefab {address} !");
                throw new();
            }

            _prefabs.Add(hash, result.gameObject);

            return result.GetComponent<T>();
        }

        public T GetAsset<T>(string address) where T : Object
        {
            int hash = Animator.StringToHash(address);
            if (_assets.ContainsKey(hash)) return (T)_assets[hash];

            var result = Resources.Load<T>(address);


            if (result == null)
            {
                Debug.LogError($"not exist {typeof(T).Name} at {address} !");
                throw new();
            }

            _assets.Add(hash, result);
            return (T)_assets[hash];
        }

        public void Dispose()
        {
            _prefabs.Clear();
            _assets.Clear();
        }
    
    }
}