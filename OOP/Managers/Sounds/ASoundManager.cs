using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Xynok.OOP.Data.Core.Dictionary;
using Xynok.Patterns.Singleton;

namespace Xynok.OOP.Managers.Sounds
{
    [Serializable]
    public class SoundDict<T> : SerializableDictionary<T, SoundData> where T : Enum
    {
    }

    [Serializable]
    public class SoundData
    {
        public AudioClip sound;
        [Range(.03f, 1f)] public float volume;
    }

    [RequireComponent(typeof(AudioSource))]
    public abstract class ASoundManager<T> : ASingleton<ASoundManager<T>> where T : Enum
    {
        [ReadOnly] [SerializeField] private AudioSource audioSource;
        [SerializeField] private SoundDict<T> soundDict;
        private T _currentSound;

        private void OnValidate()
        {
            if (!audioSource) audioSource = GetComponent<AudioSource>();
        }

        public void Play(T soundID)
        {
            if (!CanPlay(soundID)) return;
            audioSource.clip = soundDict[soundID].sound;
            audioSource.volume = soundDict[soundID].volume;
            
            _currentSound = soundID;
            audioSource.Play();
        }

        bool CanPlay(T soundID)
        {
            bool exist = soundDict.ContainsKey(soundID);
            bool isPlaying = audioSource.isPlaying;
            bool isSameSound = _currentSound.Equals(soundID);
            if (!exist)
            {
                Debug.LogError($"does not exist sound of {soundID}");
                return false;
            }

            if (isPlaying && isSameSound) return false;

            return true;
        }
    }
}