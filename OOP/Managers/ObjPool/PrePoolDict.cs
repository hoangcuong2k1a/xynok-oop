﻿using System;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Dictionary;

namespace Xynok.OOP.Managers.ObjPool
{
    [Serializable]
    public class PrePoolDict<T>: SerializableDictionary<T, PoolData> where T: Enum
    {
    }
    
    [Serializable]
    public class PoolData
    {
        public string path;
        public bool recycle;
        public int capacity;
        public int preload;
    }
}