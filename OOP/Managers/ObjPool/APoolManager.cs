using System;
using System.Collections.Generic;
using ALCCore.Managers;
using Cysharp.Threading.Tasks;
using Lean.Pool;
using UnityEngine;
using Xynok.Patterns.Singleton;
using Zk1Core.Managers.ObjPool.APIs;

namespace Xynok.OOP.Managers.ObjPool
{
    public class APoolManager<T>: ASingleton<APoolManager<T>> where T: Enum
    {
       [SerializeField] private PrePoolDict<T> prePoolDict;
        private Dictionary<T, LeanGameObjectPool> _pools = new();

        private Action _onDispose;

        protected virtual void Start()
        {
            Init();
        }

        async void Init()
        {
            if (prePoolDict.Count > 0)
            {
                await UniTask.WaitUntil(() => AssetManager.Instance != null);
                foreach (var obj in prePoolDict)
                {
                    CreatePool(obj.Key, obj.Value);
                }
            }
        }

        public void Spawn(T resourceID, Vector3 spawnPos, ISpawner spawner = null)
        {
            // check if resource's pool existed
            if (!_pools.ContainsKey(resourceID))
            {
                Debug.LogError($"not exist pool for {resourceID}");
                return;
            }

            // spawn resource
            var obj = _pools[resourceID].Spawn(null);
            obj.gameObject.transform.position = spawnPos;

            // sync data with current world
            Physics.SyncTransforms();

            // set data for resource if sender existed
            if (spawner == null) return;
            ISpawnable injectable = obj.GetComponent<ISpawnable>();

            if (injectable == null)
                Debug.LogError($"not existed dependency of {spawner.GetType().Name} on prefab {resourceID}");
            else injectable.SetDependency(spawner);
        }

        public void RemovePool(T resourceID)
        {
            if (!_pools.ContainsKey(resourceID))
            {
                Debug.LogError($"does not exist pool of {resourceID}");
                return;
            }

            _onDispose -= _pools[resourceID].DespawnAll;
            _pools[resourceID].DespawnAll();
            LeanPool.Links.Remove(gameObject);
            Destroy(_pools[resourceID].gameObject);
            _pools.Remove(resourceID);
        }

         void CreatePool(T resourceID, PoolData poolData)
        {
            if (_pools.ContainsKey(resourceID))
            {
                Debug.LogWarning($"pool of {resourceID} existed !");
                return;
            }

            GameObject prefab = AssetManager.Instance.GetAsset<GameObject>($"{poolData.path}{resourceID}");

            LeanGameObjectPool pool = new GameObject($"pool of {resourceID}").AddComponent<LeanGameObjectPool>();
            pool.transform.SetParent(transform);

            pool.Prefab = prefab;
            pool.Capacity = poolData.capacity;
            pool.Recycle = poolData.recycle;
            pool.Preload = poolData.preload;
            pool.PreloadAll();
            _pools.Add(resourceID, pool);
            _onDispose += pool.DespawnAll;
        }

        public void Dispose()
        {
            _onDispose?.Invoke();
            _onDispose = default;
        }
    }
}