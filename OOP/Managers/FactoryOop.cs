﻿using ALCCore.Enums;
using ALCCore.Patterns.Factory.Reflection;
using ALCCore.Services.Abilities.Core;
using Xynok.Core.Services.Abilities.Core;
using Xynok.Patterns.Singleton;
using Zk1Core.Patterns.Singleton;

namespace ALCCore.Managers
{
    public class FactoryOop : ALazySingleton<FactoryOop>
    {
        public AbilityFactory AbilityFactory;

        void Awake()
        {
            Init();
        }

        void Init()
        {
            AbilityFactory = new AbilityFactory();
        }
    }
}