﻿using ALCCore.Enums;
using UnityEngine;
using Xynok.OOP.Managers.GUI;
using Xynok.Utils;

namespace Xynok.OOP.GUI.Pages
{
    public class PageLauncher : MonoBehaviour
    {
        public float timeDelay = 1.5f;
        private ActionDuration _actionDuration;

        private void Start()
        {
            _actionDuration = new();
            _actionDuration.Start(LoadMainGame, timeDelay);
        }

        void LoadMainGame()
        {
            SceneController.Instance.LoadScene(SceneType.Gameplay, null);
        }
    }
}