using ALCCore.Managers;
using UnityEngine;
using UnityEngine.UI;
using Xynok.OOP.Managers;
using Zk1Core.Managers;

namespace Zk1Core.GUI.View.Img
{
    public class ImageBase: MonoBehaviour
    {
        public Image img;

        public virtual void SetData(string pathToImage)
        {
            img.sprite = AssetManager.Instance.GetAsset<Sprite>(pathToImage);
        }

    }
}