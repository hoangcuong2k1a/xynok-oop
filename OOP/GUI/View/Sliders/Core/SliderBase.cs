﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Xynok.OOP.GUI.View.Texts.Core;

namespace Xynok.OOP.GUI.View.Sliders.Core
{
    public struct SliderData
    {
        public string Title;
        public float Min;
        public float Max;
        public float Value;
    }

    [RequireComponent(typeof(Slider))]
    public class SliderBase : MonoBehaviour
    {
        [ReadOnly] [SerializeField] protected Slider slider;
        [SerializeField] protected TextBase titleTxt;
        [SerializeField] protected TextBase valueTxt;
        private Action<float> onValueChanged;

        private void OnValidate()
        {
            if (!slider) slider = GetComponentInChildren<Slider>();
        }

        private void Start()
        {
            slider.onValueChanged.AddListener(OnChanged);
        }

        public void AddListener(Action<float> action)
        {
            onValueChanged -= action;
            onValueChanged += action;
        }

        public void RemoveListener(Action<float> action)
        {
            onValueChanged -= action;
        }

        public void SetValue(float value)
        {
            slider.value = value;
        }

        public void SetData(SliderData data)
        {
            titleTxt.SetValue(data.Title);
            slider.minValue = data.Min;
            slider.maxValue = data.Max;
            slider.value = data.Value;
            SetValue(data.Value);
            valueTxt.SetValue(data.Value);
        }

        void OnChanged(float value)
        {
            onValueChanged?.Invoke(value);
            valueTxt.SetValue(value);
        }

        void Dispose()
        {
            slider.onValueChanged.RemoveListener(OnChanged);
            onValueChanged = default;
        }

        private void OnDestroy()
        {
            Dispose();
        }
    }
}