using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Xynok.OOP.GUI.View.Texts.Core
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextBase : MonoBehaviour
    {
        public TextMeshProUGUI txt;


        private void OnValidate()
        {
            if (!txt) txt = GetComponent<TextMeshProUGUI>();
        }

        public void SetValue(string value)
        {
            txt.text = value;
        }

        [Button]
        public void SetValue(float value, string format = "F")
        {
            txt.text = value.ToString(format);
        }
    }
}