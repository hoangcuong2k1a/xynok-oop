﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Xynok.OOP.GUI.View.Texts.Core;

namespace Xynok.GUI.View.Toggles.Core 
{
    [RequireComponent(typeof(Toggle))]
    public class ToggleBase : MonoBehaviour
    {
        [SerializeField] [ReadOnly] private Toggle toggle;
        [SerializeField] private TextBase toggleLabelTxt;
        protected Action<bool> _onValueChanged;

        private void OnValidate()
        {
            if (!toggle) toggle = GetComponent<Toggle>();
        }

        private void Start()
        {
            toggle.onValueChanged.AddListener(OnValueChanged);
        }

        public void SetValue(bool value)
        {
            toggle.isOn = value;
        }

        public void AddListener(Action<bool> action)
        {
            _onValueChanged -= action;
            _onValueChanged += action;
        }

        public void RemoveListener(Action<bool> action)
        {
            _onValueChanged -= action;
        }

        public  void SetData(string label, bool isOn)
        {
            toggle.onValueChanged.AddListener(OnValueChanged);
            toggle.isOn = isOn;
            if (toggleLabelTxt) toggleLabelTxt.SetValue(label);
        }

        private void OnValueChanged(bool isOn)
        {
            _onValueChanged?.Invoke(isOn);
        }

        void Dispose()
        {
            toggle.onValueChanged.RemoveListener(OnValueChanged);
        }

        private void OnDestroy()
        {
            Dispose();
        }
    }
}