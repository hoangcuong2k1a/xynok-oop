﻿using System;
using System.Linq;
using TMPro;
using UnityEngine;
using Xynok.APIs;

namespace ALCCore.GUI.View.Dropdowns.Core
{
    public struct DropdownDependency
    {
        public string[] Options;
        public Action<int> OnValueChanged;
        public int DefaultIndex;
    }

    [RequireComponent(typeof(TMP_Dropdown))]
    public class DropdownBase : MonoBehaviour, IBinder<int>, IInjectable<DropdownDependency>
    {
        [SerializeField] protected TMP_Dropdown dropdown;
        protected Action<int> ValueChanged;

        private void OnValidate()
        {
            if (!dropdown) dropdown = GetComponent<TMP_Dropdown>();
        }

        private void Start()
        {
            dropdown.onValueChanged.AddListener(InvokeValueChanged);
        }

        void InvokeValueChanged(int value)
        {
            ValueChanged?.Invoke(value);
        }

        public void AddListener(Action<int> listener)
        {
            ValueChanged -= listener;
            ValueChanged += listener;
        }

        public void RemoveListener(Action<int> listener)
        {
            ValueChanged -= listener;
        }

        public void SetDependency(DropdownDependency dependency)
        {
            Dispose();
            dropdown.onValueChanged.AddListener(InvokeValueChanged);
            
            TMP_Dropdown.OptionData[] options = new TMP_Dropdown.OptionData[dependency.Options.Length];

            for (int i = 0; i < dependency.Options.Length; i++)
            {
                options[i] = new TMP_Dropdown.OptionData(dependency.Options[i]);
            }

            dropdown.options = options.ToList();
            dropdown.value = dependency.DefaultIndex < 0 ? 0 : dependency.DefaultIndex;

            AddListener(dependency.OnValueChanged);
        }

        public void Dispose()
        {
            dropdown.ClearOptions();
            dropdown.onValueChanged.RemoveListener(InvokeValueChanged);
            ValueChanged = default;
        }

        private void OnDestroy()
        {
            Dispose();
        }
    }
}