﻿using ALCCore.Data.Core.Collections;
using UnityEngine;
using Xynok.APIs;
using Zk1Core.Data.Core.Collections;

namespace Zk1Core.GUI.View.Scrollers
{
    public abstract class AScrollerController<T> : MonoBehaviour, IInjectable<T> where T : ICollectionDataBinding
    {
        protected T collection;

        public void SetDependency(T dependency)
        {
            if (collection != null) Dispose();
            collection = dependency;
            Init();
        }

        protected abstract void Init();
        protected abstract void OnDispose();

        public void Dispose()
        {
            OnDispose();
        }
    }
}