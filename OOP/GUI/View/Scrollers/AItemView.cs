﻿using System;
using ALCCore.Data;
using ALCCore.Enums;
using ALCCore.GUI.View.Btn.Core;
using UnityEngine;
using Xynok.APIs;
using Xynok.Enums;
using Xynok.OOP.Data;
using Xynok.OOP.Data.Core;
using Xynok.OOP.Data.Primitives;
using Xynok.OOP.GUI.View.Texts.Core;
using Zk1Core.Data;
using Zk1Core.GUI.View.Img;

namespace Zk1Core.GUI.View.Scrollers
{
    [RequireComponent(typeof(ButtonBase))]
    public abstract class AItemView<T> : MonoBehaviour, IInjectable<T> where T : ResourceBase
    {
        [SerializeField] protected ImageBase selectedImg;
        [SerializeField] protected TextBase nameTxt;
        [SerializeField] protected ButtonBase btn;
        protected T Data;

        private BoolData _selected;

        public event Action<T> OnClick;

        protected virtual void OnValidate()
        {
            if (!btn) btn = GetComponent<ButtonBase>();
        }

        public void SetDependency(T dependency)
        {
            if (Data != null) Dispose();
            Data = dependency;

            nameTxt.SetValue(Data.resourceID.ToString());

            _selected = Data.GetState(BoolDataType.Selected);
            OnSelectedChanged(_selected.Value);
            _selected.AddListener(OnSelectedChanged);
            btn.AddListener(Click);

            Init();
        }

        void Click()
        {
            _selected.Value = !_selected.Value;
            OnClick?.Invoke(Data);
        }

        void OnSelectedChanged(bool value)
        {
            if (selectedImg) selectedImg.gameObject.SetActive(value);
            else Debug.LogWarning($"Selected image not found in {name}");
        }

        protected abstract void Init();
        protected abstract void OnDispose();

        public void Dispose()
        {
            btn.RemoveListener(Click);
            _selected?.RemoveListener(OnSelectedChanged);
            OnDispose();
        }
    }
}