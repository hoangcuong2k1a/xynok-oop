﻿using UnityEngine;
using Xynok.APIs;
using Xynok.GUI.View.Toggles.Core;
using Xynok.OOP.Data.Primitives;

namespace Xynok.OOP.GUI.View.Data
{
    public class BoolDataView : MonoBehaviour, IInjectable<BoolData>
    {
        [SerializeField] private ToggleBase toggleBase;
        [SerializeField] private BoolData boolData;

        [SerializeField] private bool testOnInspector;

        private void Start()
        {
            if (testOnInspector) SetDependency(boolData);
        }

        public void SetDependency(BoolData dependency)
        {
            Dispose();
            boolData = dependency;
            toggleBase.SetData(boolData.Key.ToString(), boolData.Value);
         
            boolData.AddListener(UpdateToggleValue);
            toggleBase.AddListener(UpdateBoolDataValue);
            gameObject.SetActive(true);
        }

        void UpdateBoolDataValue(bool value)
        {
            boolData.Value = value;
        }

        void UpdateToggleValue(bool value)
        {
            toggleBase.SetValue(value);
        }

        public void Dispose()
        {
            boolData?.RemoveListener(UpdateToggleValue);
            toggleBase.RemoveListener(UpdateBoolDataValue);
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            Dispose();
        }
    }
}