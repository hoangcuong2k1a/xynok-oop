﻿using UnityEngine;
using Xynok.APIs;
using Xynok.OOP.Data.Core.Primitives;
using Xynok.OOP.Data.Primitives;

namespace Xynok.OOP.GUI.View.Data
{
    // TODO: update this later
    public class Vector3DataView : MonoBehaviour, IInjectable<Vector3Data>
    {
        [SerializeField] protected FloatDataView xValue;
        [SerializeField] protected FloatDataView yValue;
        [SerializeField] protected FloatDataView zValue;
        private Vector3Data _source;

        private FloatValue _xData;
        private FloatData _yData;
        private FloatData _zData;
        public void SetDependency(Vector3Data dependency)
        {
            if (_source != null) Dispose();
            _source = dependency;
            _source.AddListener(OnSourceChanged);
        }

        void OnSourceChanged(Vector3 newValue)
        {
        }

        void UpdateView(Vector3 value)
        {
            
        }

        public void Dispose()
        {
            _source?.RemoveListener(OnSourceChanged);
        }
    }
}