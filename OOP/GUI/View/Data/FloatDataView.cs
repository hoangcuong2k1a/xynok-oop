﻿using UnityEngine;
using Xynok.APIs;
using Xynok.OOP.Data.Primitives;
using Xynok.OOP.GUI.View.Sliders.Core;

namespace Xynok.OOP.GUI.View.Data
{
    public class FloatDataView : MonoBehaviour, IInjectable<FloatData>
    {
        [SerializeField] private SliderBase sliderBase;
        [SerializeField] private FloatData floatData;
        [SerializeField] private bool testOnInspector;

        private void Start()
        {
            if (testOnInspector) SetDependency(floatData);
        }

        public void SetDependency(FloatData dependency)
        {
            Dispose();
            floatData = dependency;
            sliderBase.SetData(new SliderData
            {
                Title = floatData.Key.ToString(),
                Min = -1f,
                Max = 9f,
                Value = floatData.Value
            });
            floatData.AddListener(UpdateSliderValue);
            sliderBase.AddListener(UpdateFloatDataValue);
            gameObject.SetActive(true);
        }

        void UpdateFloatDataValue(float value)
        {
            floatData.Value = value;
        }

        void UpdateSliderValue(float value)
        {
            sliderBase.SetValue(value);
        }

        public void Dispose()
        {
            floatData?.AddListener(UpdateSliderValue);
            sliderBase.RemoveListener(UpdateFloatDataValue);
            gameObject.SetActive(false);
        }
        
        private void OnDestroy()
        {
            Dispose();
        }
    }
}