﻿using UnityEngine;
using Xynok.APIs;
using Xynok.GUI.View.Toggles.Core;
using Xynok.OOP.Data.Primitives;

namespace Xynok.OOP.GUI.View.Data
{
    public class TriggerDataView: MonoBehaviour, IInjectable<TriggerData>
    {
        [SerializeField] private ToggleBase toggleBase;
        [SerializeField] private TriggerData triggerData;

        [SerializeField] private bool testOnInspector;

        private void Start()
        {
            if (testOnInspector) SetDependency(triggerData);
        }

        public void SetDependency(TriggerData dependency)
        {
            Dispose();
            triggerData = dependency;
            toggleBase.SetData(triggerData.Key.ToString(), triggerData.Value);
         
            triggerData.AddListener(UpdateToggleValue);
            toggleBase.AddListener(UpdateBoolDataValue);
            gameObject.SetActive(true);
        }

        void UpdateBoolDataValue(bool value)
        {
            triggerData.Value = value;
        }

        void UpdateToggleValue(bool value)
        {
            toggleBase.SetValue(value);
        }

        public void Dispose()
        {
            triggerData?.RemoveListener(UpdateToggleValue);
            toggleBase.RemoveListener(UpdateBoolDataValue);
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            Dispose();
        }
    }
}