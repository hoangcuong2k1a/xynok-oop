using ALCCore.GUI.View.Btn.Core;
using Xynok.OOP.GUI.View.Texts.Core;

namespace ALCCore.GUI.View.Btn
{
    public class ButtonText : ButtonBase
    {
        public TextBase btnTitle;

        public void SetBtnTitle(string value)
        {
            btnTitle.SetValue(value);
        }
    }
}