﻿using System;
using ALCCore.Enums;
using ALCCore.GUI.View.Btn.Core;
using Core.Runtime.Data.Enums;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using VeThanCore.Data.Enums;
using Xynok.Enums;
using Xynok.OOP.Managers.GUI;
using Zk1Core.Managers.GUI;
using Zk1Core.Runtime.Managers.GUI;

namespace Zk1Core.Runtime.GUI.View.Btn.GUINavigator
{
    public class ButtonGUINavigator : ButtonBase
    {
        public GUIType targetType;
        [ShowIf("targetType", GUIType.Scene)] public SceneType targetScene;
        [ShowIf("targetType", GUIType.Page)] public PageType targetPage;
        [ShowIf("targetType", GUIType.Modal)] public ModalType targetModal;

        protected override void Start()
        {
            base.Start();
            AddListener(LoadTargetGUI);
        }

        async void LoadTargetGUI()
        {
            if (targetType == GUIType.QuitGame)
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#endif
                Application.Quit();
            }

            if (targetType == GUIType.Scene)
            {
                // delay or do something before load ...

                GUIController.Instance.LoadPage(PageType.LoadingFullScreen);
                await UniTask.Delay(TimeSpan.FromSeconds(1.5f));
                SceneController.Instance.LoadScene(targetScene, null);
            }

            if (targetType == GUIType.Page)
            {
                GUIController.Instance.LoadPage(targetPage);
            }

            if (targetType == GUIType.Modal)
            {
                GUIController.Instance.LoadModal(targetModal);
            }
        }
    }
}