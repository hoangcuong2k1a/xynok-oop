using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ALCCore.GUI.View.Btn.Core
{
    [RequireComponent(typeof(Button))]
    public class ButtonBase : MonoBehaviour, ISelectHandler, IDeselectHandler
    {
        [ReadOnly] public Button btn;
        [SerializeField] private bool selectOnActive;
        public UnityEvent onClicked;
        public UnityEvent onSelected;
        public UnityEvent onDeSelected;

        private Action _onClicked;
        private void OnValidate()
        {
            if (btn == null) btn = GetComponent<Button>();
        }


        protected virtual void Start()
        {
            Init();

            if (selectOnActive) btn.Select();
        }


        void Init()
        {
            btn.onClick.AddListener(() =>
            {
                _onClicked?.Invoke();
                onClicked?.Invoke();
            });
        }


        public void AddListener(Action action)
        {
            _onClicked -= action;
            _onClicked += action;
        }

        public void RemoveListener(Action action)
        {
            _onClicked -= action;
        }

        public void RemoveAllListener()
        {
            onClicked = default;
            _onClicked = default;
        }

        protected virtual void OnDestroy()
        {
            RemoveAllListener();
        }

        public virtual void OnSelect(BaseEventData eventData)
        {
            onSelected?.Invoke();
        }

        public virtual void OnDeselect(BaseEventData eventData)
        {
            onDeSelected?.Invoke();
        }
    }
}