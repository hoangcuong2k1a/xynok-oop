﻿using UnityEngine;
using Xynok.OOP.Data.Core.Utils;
using Xynok.OOP.GUI.View.Texts.Core;

namespace ALCCore.GUI.Utils
{
    public class InputCapture : MonoBehaviour
    {
        [SerializeField] private TextBase txt;
        [SerializeField] private int maxStackCapture = 3;

        private string[] _currentInput;
        private int _counter;

        private KeyCode[] _keyCodes;

        private void Start()
        {
            _currentInput = new string[maxStackCapture];
            _counter = 0;
            _keyCodes = EnumHelper.GetAllValuesOf<KeyCode>();
        }

        private void Update()
        {
            if (Input.anyKeyDown)
            {
                for (int i = 0; i < _keyCodes.Length; i++)
                {
                    if (Input.GetKey(_keyCodes[i]))
                    {
                        _currentInput[_counter] = _keyCodes[i].ToString();
                        _counter++;
                        if (_counter >= maxStackCapture) _counter = 0;
                        UpdateTextView();
                        break;
                    }
                }
            }
        }

        void UpdateTextView()
        {
            string output = "";
            for (int i = 0; i < _currentInput.Length; i++)
            {
                output += $"{_currentInput[i]}  ";
            }

            txt.SetValue(output);
        }
    }
}