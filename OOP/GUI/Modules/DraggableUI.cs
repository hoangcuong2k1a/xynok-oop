﻿using Lean.Gui;
using UnityEngine;

namespace Zk1Core.GUI.Modules
{
    [RequireComponent(typeof(LeanDrag))]
    public class DraggableUI : MonoBehaviour
    {
        public LeanDrag leanDrag;
        public bool revertOnEnd = true;
        private RectTransform _rectTransform;
        private Vector3 _originPos;

        private void Start()
        {
            _rectTransform = leanDrag.TargetTransform;
            leanDrag.OnBegin.AddListener(OnBegin);
            leanDrag.OnEnd.AddListener(OnEnd);
        }


        void OnBegin()
        {
            _originPos = _rectTransform.position;
        }

        void OnEnd()
        {
            if (revertOnEnd) _rectTransform.position = _originPos;
        }
    }
}