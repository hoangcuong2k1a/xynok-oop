﻿using Core.Runtime.Data.Enums;
using Cysharp.Threading.Tasks;
using Xynok.OOP.GUI.View.Texts.Core;
using Zk1Core.Managers.GUI;
using Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal;

namespace Xynok.OOP.GUI.Modals
{
    public class ModalQuickAlert : Modal
    {
        public float lifeTime = 2f;
        public TextBase txt;
        public void SetAlert(string value) => txt.SetValue(value);
        private InputMapType _currentInputMap;


        public override void DidPushEnter()
        {
            base.DidPushEnter();
            Invoke(nameof(Hide), lifeTime);
        }

        async void Hide()
        {
            await GUIController.Instance.modalContainer.Pop(true); // hide this
        }
    }
}