﻿using System;
using UnityEngine;
using Xynok.APIs;
using Xynok.Utils;
using Zk1Core.Managers;
using Zk1Core.Managers.GUI;
using Zk1Core.Utils;

namespace Zk1Core.Inputs.Mouse.Click
{
    public interface IOnMouseClicked
    {
        void OnMouseClicked(GameObject obj);
    }

    [Serializable]
    public class MouseClickHandler : IInjectable<IOnMouseClicked>
    {
        [SerializeField] private float raycastDistance = 100f;
        private IOnMouseClicked _clicker;
        private Camera _cam;


        public void SetDependency(IOnMouseClicked dependency)
        {
            if (_clicker != null) Dispose();
            _cam ??= GUIController.Instance ? GUIController.Instance.mainCam : Camera.main;
            _clicker = dependency;
            TimeCycle.Instance.AddListenerOnUpdate(CastClick);
        }

        void CastClick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit raycastHit;
                Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out raycastHit, raycastDistance))
                {
                    if (raycastHit.transform != null) _clicker.OnMouseClicked(raycastHit.transform.gameObject);
                }
            }
        }


        public void Dispose()
        {
            if (TimeCycle.Instance) TimeCycle.Instance.RemoveListenerOnUpdate(CastClick);
        }
    }
}