﻿using UnityEngine;
using Xynok.APIs;
using Zk1Core.Inputs.Mouse.APIs;

namespace Xynok.OOP.Inputs.Mouse.Click
{
    public class Clickable : MonoBehaviour, IInjectable<IMouseClickable>
    {
        private IMouseClickable _clickable;

        public void SetDependency(IMouseClickable dependency)
        {
            if (_clickable != null) Dispose();
            _clickable = dependency;
        }

        private void OnMouseDown()
        {
            if (!_clickable.Clickable) return;
            _clickable.WhileMouseDown();
        }

        private void OnMouseUp()
        {
            if (!_clickable.Clickable) return;
            _clickable.WhileMouseUp();
        }

        public void Dispose()
        {
        }
    }
}