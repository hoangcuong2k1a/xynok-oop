﻿using UnityEngine;
using Zk1Core.Managers.GUI;

namespace Xynok.OOP.Inputs.Mouse
{
    public class MouseRaycastHandler
    {
        public float raycastDistance = 100f;
        private Camera _cam;

        public GameObject GetObjRaycast(LayerMask castLayer)
        {
            _cam ??= GUIController.Instance ? GUIController.Instance.mainCam : Camera.main;
            if (_cam == null)
            {
                Debug.LogError("MouseRaycastHandler: _cam is null");
                return null;
            }

            RaycastHit raycastHit;
            Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out raycastHit, raycastDistance, castLayer.value))
            {
                if (raycastHit.transform != null) return raycastHit.transform.gameObject;
            }

            return null;
        }
    }
}