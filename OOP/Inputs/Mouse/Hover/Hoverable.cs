﻿using UnityEngine;
using Xynok.APIs;
using Zk1Core.Inputs.Mouse.APIs;

namespace Zk1Core.Inputs.Mouse.Hover
{
   public class Hoverable : MonoBehaviour, IInjectable<IMouseHoverable>
    {
        private IMouseHoverable _hoverable;

        public void SetDependency(IMouseHoverable dependency)
        {
            if (_hoverable != null) Dispose();
            _hoverable = dependency;
        }

        private void OnMouseEnter()
        {
            _hoverable?.WhileMouseEnter();
        }

        private void OnMouseExit()
        {
            _hoverable?.WhileMouseExit();
        }

        private void OnMouseOver()
        {
            _hoverable?.WhileMouseOver();
        }

        public void Dispose()
        {
            
        }
    }
}