﻿using System;
using UnityEngine;
using Xynok.OOP.APIs;
using Zk1Core.Inputs.Mouse.Drag;

namespace Zk1Core.Inputs.Mouse.APIs
{
    public interface IMouseDraggable: IMono
    {
        bool Draggable { get; set; }
        MouseDragType DragType { get; }
        Transform DraggerTransform { get; }
        void WhileMouseDrag();
        void WhileMouseDrop();
        
        event Action OnDrag;
        event Action OnDrop;
    }

    public interface IMouseDraggable<out T> : IMouseDraggable
    {
        event Action<T> OnDragOwner;
        event Action<T> OnDropOwner;
    }
}