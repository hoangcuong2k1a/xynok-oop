﻿using System;
using ALCCore.Entities.APIs;
using Xynok.OOP.APIs;
using Zk1Core.Entities.APIs;
using Zk1Core.Services.Abilities.APIs;

namespace Zk1Core.Inputs.Mouse.APIs
{
    public interface IMouseHoverable: IMono,IPosition
    {
        void WhileMouseEnter();
        void WhileMouseExit();
        void WhileMouseOver();
        event Action OnMouseEnter;
        event Action OnMouseExit;
        event Action OnMouseOver;
    }

    public interface IMouseHoverable<out T> : IMouseHoverable
    {
        event Action<T> OnMouseEnterObj;
        event Action<T> OnMouseExitObj;
        event Action<T> OnMouseOverObj;
    }
}