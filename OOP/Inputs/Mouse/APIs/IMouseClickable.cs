﻿using System;

namespace Zk1Core.Inputs.Mouse.APIs
{
    public interface IMouseClickable
    {
        bool Clickable { get; set; }
        void WhileMouseDown();
        void WhileMouseUp();
        event Action OnMouseDown;
        event Action OnMouseUp;
    }


    public interface IMouseClickable<out T> : IMouseClickable
    {
        event Action<T> OnMouseDownObj;
        event Action<T> OnMouseUpObj;
    }
}