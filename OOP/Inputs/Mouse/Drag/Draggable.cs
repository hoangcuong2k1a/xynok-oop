﻿using System;
using UnityEngine;
using Xynok.APIs;
using Zk1Core.Inputs.Mouse.APIs;
using Zk1Core.Managers;
using Zk1Core.Managers.GUI;

namespace Zk1Core.Inputs.Mouse.Drag
{
    public class Draggable : MonoBehaviour, IInjectable<IMouseDraggable>
    {
        private IMouseDraggable _dragger;
        private Camera _camera;
        private Vector3 OffsetPlane;

        public void SetDependency(IMouseDraggable dependency)
        {
            if (_dragger != null) Dispose();
            _camera ??= GUIController.Instance ? GUIController.Instance.mainCam : Camera.main;
            _dragger = dependency;
        }

        public virtual void Dispose()
        {
        }
        
        private void OnMouseUp()
        {
            _dragger?.WhileMouseDrop();
        }

        private void OnMouseDrag()
        {
            if (_dragger == null)
            {
                Debug.LogError("Draggable: _dragger is null");
                return;
            }

            if (!_dragger.Draggable) return;

            _dragger.WhileMouseDrag();

            if (_dragger.DragType == MouseDragType.Plane) PlaneDrag();
            if (_dragger.DragType == MouseDragType.ThreeD) ThreeDDrag();
        }

        void ThreeDDrag()
        {
            if (_dragger == null)
            {
                Debug.LogError("Draggable: _dragger is null");
                return;
            }

            float distanceToScreen = _camera.WorldToScreenPoint(_dragger.DraggerTransform.position).z;
            _dragger.DraggerTransform.position =
                _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,
                    distanceToScreen));
        }

        void PlaneDrag()
        {
            if (_dragger == null)
            {
                Debug.LogError("Draggable: _dragger is null");
                return;
            }

            var position = _dragger.DraggerTransform.position;
            float distanceToScreen = _camera.WorldToScreenPoint(position).z;
            Vector3 posMove =
                _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,
                    distanceToScreen));
            position = new Vector3(posMove.x, OffsetPlane.y, posMove.z);
            _dragger.DraggerTransform.position = position;
        }
    }
}