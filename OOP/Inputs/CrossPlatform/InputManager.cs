using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using Xynok.OOP.Inputs.CrossPlatform.Core;
using Xynok.Patterns.Singleton;
using InputActionType = Xynok.OOP.Inputs.CrossPlatform.Core.InputActionType;

namespace Xynok.OOP.Inputs.CrossPlatform
{
    [RequireComponent(typeof(PlayerInput))]
    public class InputManager : ASingleton<InputManager>
    {
        [ReadOnly] [SerializeField] private PlayerInput playerInput;
        [SerializeField] private InputActionAsset inputActionAsset;
        [SerializeField] private InputMap[] inputMaps;

        private Action _onDispose;

        private void OnValidate()
        {
            if (!playerInput) playerInput = GetComponent<PlayerInput>();
            if (playerInput && inputActionAsset) playerInput.actions = inputActionAsset;
        }

        protected override void Awake()
        {
            base.Awake();
            Init();
        }

        public InputActionBase GetInput(InputMapType map, InputActionType action)
        {
            var inputMap = inputMaps.FirstOrDefault(e => e.name == map);
            if (inputMap != null) return inputMap.GetInputAction(action);

            Debug.LogError($"inputMap {map} does not exist");
            return null;
        }

        void Init()
        {
            if (inputActionAsset == null)
            {
                Debug.LogError("inputActionAsset is null");
                return;
            }

            // init maps
            inputMaps = new InputMap[inputActionAsset.actionMaps.Count];

            for (int i = 0; i < inputActionAsset.actionMaps.Count; i++)
            {
                var map = inputActionAsset.actionMaps[i];

                inputMaps[i] ??= new();
                inputMaps[i].SetDependency(map);

                _onDispose += inputMaps[i].Dispose;
            }
        }

        private void OnDestroy()
        {
            _onDispose?.Invoke();
        }
    }
}