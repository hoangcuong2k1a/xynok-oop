using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using Xynok.APIs;
using Xynok.OOP.Data.Core.Utils;

namespace Xynok.OOP.Inputs.CrossPlatform.Core
{
    [Serializable]
    public class InputActionBase : IInjectable<InputAction>
    {
        [ReadOnly] public InputActionType name;
        public event Action OnStarted;
        public event Action OnPerformed;
        public event Action OnCanceled;
        public event Action<InputAction.CallbackContext> OnContextStarted;
        public event Action<InputAction.CallbackContext> OnContextPerformed;
        public event Action<InputAction.CallbackContext> OnContextCanceled;

        protected InputAction Source;

        public void Update()
        {
        }

        public void SetDependency(InputAction dependency)
        {
            if (Source != null) Dispose();
            Source = dependency;
            if (Source == null)
            {
                Debug.LogError($"InputAction {name} has Source null");
                return;
            }

            Init();
        }


        protected virtual void Init()
        {
            name = EnumHelper.ParseEnum<InputActionType>(Source.name);

            Source.started -= Started;
            Source.started += Started;
            
            Source.performed -= Performed;
            Source.performed += Performed;

            Source.canceled -= Canceled;
            Source.canceled += Canceled;
        }

        void Started(InputAction.CallbackContext context)
        {
            OnContextStarted?.Invoke(context);
            OnStarted?.Invoke();
        }
        void Performed(InputAction.CallbackContext context)
        {
            OnContextPerformed?.Invoke(context);
            OnPerformed?.Invoke();
        }
        void Canceled(InputAction.CallbackContext context)
        {
            OnContextCanceled?.Invoke(context);
            OnCanceled?.Invoke();
        }

        protected virtual void OnDispose()
        {
            Source.started -= Started;
            Source.performed -= Performed;
            Source.canceled -= Canceled;
            
            OnStarted = default;
            OnContextStarted = default;
            
            OnCanceled = default;
            OnContextCanceled = default;
            
            OnPerformed = default;
            OnContextPerformed = default;
            name = default;
        }

        public void Dispose()
        {
            OnDispose();
        }
    }
}