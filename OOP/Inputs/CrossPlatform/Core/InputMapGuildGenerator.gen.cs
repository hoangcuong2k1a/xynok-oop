

namespace Xynok.OOP.Inputs.CrossPlatform.Core
{


public enum InputMapType{
    Gameplay,        
    GUI,        
    None,        
}


public enum InputActionType{
    Movement,        
    Attack,        
    Escape,        
}

}