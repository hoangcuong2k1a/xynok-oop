using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using Xynok.APIs;
using Xynok.OOP.Data.Core.Utils;

namespace Xynok.OOP.Inputs.CrossPlatform.Core
{
    [Serializable]
    public class InputMap : IInjectable<InputActionMap>
    {
        [ReadOnly] public InputMapType name;
        [SerializeField] private InputActionBase[] actions;
        private InputActionMap _sourceMap;
        private Action _onDispose;

        public void SetDependency(InputActionMap dependency)
        {
            if (_sourceMap != null) Dispose();
            _sourceMap = dependency;
            if (_sourceMap == null)
            {
                Debug.LogError("_sourceMap is null");
                return;
            }

            Init();
        }

        public InputActionBase GetInputAction(InputActionType actionType) =>
            actions.FirstOrDefault(e => e.name == actionType);
        void Init()
        {
            name = EnumHelper.ParseEnum<InputMapType>(_sourceMap.name);
            actions = new InputActionBase[_sourceMap.actions.Count];
            for (int i = 0; i < _sourceMap.actions.Count; i++)
            {
                int index = i;
                actions[index] ??= new();
                actions[index].SetDependency(_sourceMap.actions[index]);
                _onDispose += actions[index].Dispose;
            }
        }

        public void Dispose()
        {
            name = default;
            _onDispose?.Invoke();
        }
    }
}