using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Xynok.OOP.Inputs.CrossPlatform.Core
{
    [CreateAssetMenu(menuName = "Xynok/CodeGen/Input/EmbedInputMapDataT4")]
    public class EmbedInputMapDataT4 : ScriptableObject
    {
        public InputActionAsset inputActionAsset;

        public string[] GetAllInputMaps()
        {
            string[] result = new string[inputActionAsset.actionMaps.Count];
            for (int i = 0; i < inputActionAsset.actionMaps.Count; i++)
            {
                result[i] = inputActionAsset.actionMaps[i].name;
            }

            return result;
        }


        public string[] GetAllInputActions()
        {
            List<string> result = new();
            for (int i = 0; i < inputActionAsset.actionMaps.Count; i++)
            {
                var map = inputActionAsset.actionMaps[i];

                foreach (var action in map.actions)
                {
                    if (!result.Contains(action.name)) result.Add(action.name);
                }
            }

            return result.ToArray();
        }
    }
}