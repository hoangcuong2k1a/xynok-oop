
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;
using Xynok.OOP.Inputs.CrossPlatform.Core;
using InputActionType = Xynok.OOP.Inputs.CrossPlatform.Core.InputActionType;


namespace Xynok.OOP.Inputs.CrossPlatform.Tests
{
    public class TestNewInputSystem : MonoBehaviour
    {
        public InputMapType mapType;
        public InputActionType actionType;
        public Vector2 movementDirection;
        private InputActionBase _inputActionBase;

        private void Start()
        {
            Binding();
        }

        [Button]
        void Binding()
        {
            _inputActionBase = InputManager.Instance.GetInput(mapType, actionType);
            _inputActionBase.OnContextPerformed += StartMove;
            _inputActionBase.OnCanceled += StopMove;
        }

        void StartMove(InputAction.CallbackContext context)
        {
            var direction = context.ReadValue<Vector2>();
            movementDirection = direction;
        }
        
        void StopMove()
        {
            
            movementDirection = Vector2.zero;
        }
    }
}