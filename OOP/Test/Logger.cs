﻿using System;
using ALCCore.Data.Core.Collections;
using UnityEngine;
using Zk1Core.Data.Core.Collections;

namespace Zk1Core.Test
{
    [Serializable]
    public class IntCollection : ACollectionDataBinding<int>
    {
    }

    public class Logger : MonoBehaviour
    {
        public IntCollection collection;

        private void Start()
        {
            collection.OnChanged += OnChanged;
            collection.OnAdd += OnAdd;
            collection.OnRemove += OnRemove;
        }

        void OnAdd(int obj)
        {
            Debug.Log($"collection added: {obj}");
        }

        void OnRemove(int obj)
        {
            Debug.Log($"collection removed: {obj}");
        }

        void OnChanged(int index, int oldValue, int newValue)
        {
            Debug.Log($"collection changed: index[{index}], oldValue: {oldValue}, newValue: {newValue}");
        }
    }
}