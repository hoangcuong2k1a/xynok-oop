﻿using Sirenix.OdinInspector;
using UnityEngine;
using Xynok.Utils;

namespace Xynok.OOP.Test.Math
{
    public class TestMath : MonoBehaviour
    {
        public Transform pointA;
        public Transform pointB;
        public Transform pointC;
        public float radius = 1.2f;
        public bool forceBetweenAandB;
        public float moveSpeed = 3f;
        public bool canRun;
        public Vector3 closestPoint;

        [Button]
        void CompareDistance()
        {
            Vector3[] points = new Vector3[] { pointB.position, pointC.position };

            Vector3 closetToA = MathUtils.Compare.GetClosetPoint(pointA.position, points);
            for (int i = 0; i < points.Length; i++)
            {
                if (closetToA == points[i])
                {
                    if (i == 0) Debug.Log("B is closer to A");
                    if (i == 1) Debug.Log("C is closer to A");
                    return;
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (!canRun) return;

            Gizmos.color = Color.red;
            Gizmos.DrawLine(pointC.position, pointB.position);
            Gizmos.color = Color.green;


            closestPoint =
                MathUtils.Distance.GetClosetPointOnLine(pointA.position, pointB.position, pointC.position,
                    forceBetweenAandB);
            Gizmos.DrawLine(pointA.position, closestPoint);

            Rounded();
        }


        void Move()
        {
            // move pointA to closest point on line BC with moveSpeed
            pointA.position = Vector3.MoveTowards(pointA.position, closestPoint, moveSpeed * Time.deltaTime);
        }

        void Rounded()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(pointA.position, radius);
            Gizmos.DrawWireSphere(pointB.position, radius);
            Gizmos.DrawWireSphere(pointC.position, radius);
        }
    }
}