﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Zk1Core.Data.APIs;

namespace Xynok.OOP.Data.Core.Primitives
{
    [Serializable]
    public class APrimitiveDataBinding<T> : IPrimitiveBinder
    {
        [SerializeField] [ReadOnly] protected T baseValue;
        [SerializeField] [ReadOnly] protected T lastValue;
        [SerializeField] [ReadOnly] protected T currentValue;


        public T BaseValue => baseValue;
        public T LastValue => lastValue;
        
        private event Action<T> OnValueChanged;

        /// <summary>
        /// T1 is base value, T2 is last value, T3 is current value
        /// </summary>
        public event Action<T, T, T> OnDeepValueChanged;

        [ShowInInspector]
        public virtual T Value
        {
            get => currentValue;
            set
            {
                lastValue = currentValue;
                currentValue = value;
                EmitDeepEventChanged();
                EmitEventChanged();
            }
        }

        public APrimitiveDataBinding(T value)
        {
            baseValue = value;
            lastValue = value;
            currentValue = value;
        }

        [Button, GUIColor(0.4f, 0.8f, 1)]
        public void SetBaseValue(T value)
        {
            baseValue = value;
            lastValue = currentValue;
            currentValue = value;
            EmitEventChanged();
        }

        public void AddListener(Action<T> action)
        {
            OnValueChanged -= action;
            OnValueChanged += action;
        }

        public void RemoveListener(Action<T> action)
        {
            OnValueChanged -= action;
        }

        public void RemoveAllListener()
        {
            OnValueChanged = default;
        }

        protected virtual void EmitEventChanged()
        {
            OnValueChanged?.Invoke(currentValue);
        }

        protected virtual void EmitDeepEventChanged()
        {
            OnDeepValueChanged?.Invoke(baseValue, lastValue, currentValue);
        }
    }


    [Serializable]
    public class IntValue : APrimitiveDataBinding<int>
    {
        public IntValue(int value) : base(value)
        {
        }
    }

    [Serializable]
    public class BoolValue : APrimitiveDataBinding<bool>
    {
        public BoolValue(bool value) : base(value)
        {
        }
    }

    [Serializable]
    public class FloatValue : APrimitiveDataBinding<float>
    {
        public FloatValue(float value) : base(value)
        {
        }
    }

    [Serializable]
    public class StringValue : APrimitiveDataBinding<string>
    {
        public StringValue(string value) : base(value)
        {
        }
    }

    [Serializable]
    public class Vector2Value : APrimitiveDataBinding<Vector2>
    {
        public Vector2Value(Vector2 value) : base(value)
        {
        }
    }

    [Serializable]
    public class Vector3Value : APrimitiveDataBinding<Vector3>
    {
        public Vector3Value(Vector3 value) : base(value)
        {
        }
    }

    [Serializable]
    public class EnumValue<T> : APrimitiveDataBinding<T> where T : Enum
    {
        public EnumValue(T value) : base(value)
        {
        }
    }
}