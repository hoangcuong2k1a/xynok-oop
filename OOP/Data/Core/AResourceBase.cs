using System;
using Xynok.OOP.Data.Primitives;

namespace Xynok.OOP.Data.Core
{
    [Serializable]
    public abstract class AResourceBase<T1, T2, T3, T4, T5, T6, T7, T8>
        where T1 : Enum
        where T2 : Enum
        where T3 : AMappingDataBindingForRuntime<T4, float>
        where T4 : Enum
        where T5 : AMappingDataBindingForRuntime<T6, bool>
        where T6 : Enum
        where T7 : ATriggerData<T8>
        where T8 : Enum
    {
        public T1 resourceID;
        public T2 resourceType;

        public T3[] stats;
        public T5[] states;
        public T7[] triggers;

        protected AResourceBase(T1 resourceID, T2 resourceType)
        {
            this.resourceID = resourceID;
            this.resourceType = resourceType;
        }

        public abstract T3 GetStat(T4 floatDataType);
        public abstract T5 GetState(T6 boolDataType);
        public abstract T7 GetTrigger(T8 triggerType);
    }
}