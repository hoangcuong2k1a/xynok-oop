using System;
using System.Linq;
using ALCCore.Enums;
using Xynok.Enums;
using Xynok.OOP.Data.Primitives;

namespace Xynok.OOP.Data.Core
{
    [Serializable]
    public class ResourceBase : AResourceBase<ResourceID, ResourceType, FloatData, FloatDataType, BoolData, BoolDataType
        , TriggerData, TriggerType>
    {
       

        public ResourceBase(ResourceID resourceID, ResourceType resourceType) : base(resourceID, resourceType)
        {
        }

        public override FloatData GetStat(FloatDataType floatDataType) =>
            stats.FirstOrDefault(e => e.Key == floatDataType);

        public override BoolData GetState(BoolDataType boolDataType) =>
            states.FirstOrDefault(e => e.Key == boolDataType);

        public override TriggerData GetTrigger(TriggerType triggerType) =>
            triggers.FirstOrDefault(e => e.Key == triggerType);
    }
}