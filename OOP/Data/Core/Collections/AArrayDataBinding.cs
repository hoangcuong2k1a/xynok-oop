using System;
using ALCCore.Data;
using ALCCore.Enums;
using Core.Runtime.Data.Enums;
using Sirenix.OdinInspector;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data;
using Xynok.OOP.Data.Core;

namespace Zk1Core.Data.Core.Collections
{
    [Serializable]
    public abstract class AArrayDataBinding<T> where T : ResourceBase
    {
        public T[] elements;

        public event Action<T, int> OnChangedNewValue;
        public event Action<int> OnChangedNullValue;
        public int Length => elements.Length;

        public int IndexOf(T item)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                if (elements[i].resourceID == item.resourceID) return i;
            }

            Debug.LogError($"not exist item {item.resourceID}");
            return 0;
        }


        [Button]
        public void Init(int amount = 2)
        {
            elements = new T[amount];
        }

        public T this[int index]
        {
            get
            {
                if (index >= elements.Length)
                {
                    Debug.LogError($"index {index} out size of elements length");
                    throw new();
                }

                return elements[index];
            }
            set
            {
                if (index >= elements.Length)
                {
                    Debug.LogError($"index {index} out size of elements length");

                    return;
                }

                if (value == null || value.resourceID == ResourceID.None) OnChangedNullValue?.Invoke(index);
                else OnChangedNewValue?.Invoke(value, index);
                elements[index] = value;
            }
        }
    }
}