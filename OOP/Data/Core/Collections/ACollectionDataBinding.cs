using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ALCCore.Data.Core.Collections
{
    public interface ICollectionDataBinding
    {
    }

    [Serializable]
    public abstract class ACollectionDataBinding<T> : ICollectionDataBinding
    {
        [SerializeField] protected List<T> elements;

        public Action<T> OnAdd;
        public Action<T> OnRemove;
        public Action<int, T, T> OnChanged;

        public int Count => elements.Count;

        [ShowInInspector]
        public T this[int index]
        {
            get => elements[index];
            set
            {
                OnChanged?.Invoke(index, elements[index], value);
                elements[index] = value;
            }
        }

        public ACollectionDataBinding()
        {
            elements = new List<T>();
        }

        public void Clear() => elements.Clear();
        public int IndexOf(T item) => elements.IndexOf(item);
        public bool Contains(T item) => elements.Contains(item);


        public void RemoveListener(Action<T> action)
        {
            OnAdd -= action;
            OnRemove -= action;
        }

        public void RemoveAllListener()
        {
            OnAdd = default;
            OnRemove = default;
        }

        public virtual bool Remove(T item)
        {
            if (!elements.Contains(item))
            {
                return false;
            }

            elements.Remove(item);
            OnRemove?.Invoke(item);
            return true;
        }

        public virtual void Add(T item)
        {
            if (elements.Contains(item))
            {
                Debug.LogWarning($"duplicated element {item.GetType().Name} !");
                return;
            }

            elements.Add(item);
            OnAdd?.Invoke(item);
        }
    }
}