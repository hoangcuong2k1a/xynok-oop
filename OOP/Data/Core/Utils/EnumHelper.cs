using System;
using System.Linq;

namespace Xynok.OOP.Data.Core.Utils
{
    public struct EnumHelper
    {
        /// <returns>string to enum value</returns>
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public static T[] GetAllValuesOf<T>() where T : Enum
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToArray();
        }

    
    }
}