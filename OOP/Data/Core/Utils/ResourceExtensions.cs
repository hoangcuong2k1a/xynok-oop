using Xynok.Enums;
using Xynok.OOP.Data.Core.SubModules;
using Xynok.OOP.Data.Primitives;

namespace Xynok.OOP.Data.Core.Utils
{
    public static class ResourceExtensions
    {
        public static bool IsTheSameAs(this ResourceBase resourceBase, ResourceBase another)
        {
            if (resourceBase == null) return false;
            return resourceBase.resourceID == another.resourceID;
        }


        public static bool IsValid(this ResourceBase resourceBase)
        {
            if (resourceBase == null) return false;
            return resourceBase.resourceID != ResourceID.None;
        }

        public static ResourceActionable Clone(this ResourceActionable resourceBase)
        {
            if (resourceBase == null) return null;
            ResourceActionable newOne = new ResourceActionable(resourceBase.resourceID, resourceBase.resourceType);

            // stats and states
            ResourceBase root = Clone((ResourceBase)resourceBase);
            newOne.stats = root.stats;
            newOne.states = root.states;

            // abilities
            newOne.abilities = new AbilityCollection();
            for (int i = 0; i < resourceBase.abilities.Count; i++)
            {
                newOne.abilities.Add(resourceBase.abilities[i]);
            }

            // abilities as system
            newOne.systemAsAbilities = new SystemAsAbilityCollection();
            for (int i = 0; i < resourceBase.systemAsAbilities.Count; i++)
            {
                newOne.systemAsAbilities.Add(resourceBase.systemAsAbilities[i]);
            }

            return newOne;
        }

        public static ResourceBase Clone(this ResourceBase resourceBase)
        {
            if (resourceBase == null) return null;
            ResourceBase newOne = new ResourceBase(resourceBase.resourceID, resourceBase.resourceType);

            // stats
            newOne.stats = new FloatData[resourceBase.stats.Length];
            for (int i = 0; i < resourceBase.stats.Length; i++)
            {
                newOne.stats[i] = new(resourceBase.stats[i].Key, resourceBase.stats[i].Value);
            }

            // states
            newOne.states = new BoolData[resourceBase.states.Length];
            for (int i = 0; i < resourceBase.states.Length; i++)
            {
                newOne.states[i] = new(resourceBase.states[i].Key, resourceBase.states[i].Value);
            }

            return newOne;
        }
    }
}