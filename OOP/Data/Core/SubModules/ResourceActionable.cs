using System;
using ALCCore.Data.Core.Collections;
using ALCCore.Enums;
using Sirenix.OdinInspector;
using Xynok.Enums;
using Xynok.OOP.Data.Primitives;

namespace Xynok.OOP.Data.Core.SubModules
{
  

    [Serializable]
    public class AbilityCollection : ACollectionDataBinding<AbilityType>
    {
    }

    [Serializable]
    public class SystemAsAbilityCollection : ACollectionDataBinding<SystemAsAbilityType>
    {
    }

    [Serializable]
    public class ResourceActionableCollection : ACollectionDataBinding<ResourceActionable>
    {
    }

    [Serializable]
    public class ResourceActionable : ResourceBase
    {
        public ObjType objType;
        [ShowIf("objType", ObjType.Mono)] public AbilityCollection abilities;
        [ShowIf("objType", ObjType.Entity)] public SystemAsAbilityCollection systemAsAbilities;
        public Vector2Data[] vector2Data;
        public Vector3Data[] vector3Data;

        public ResourceActionable(ResourceID resourceID, ResourceType resourceType) : base(resourceID, resourceType)
        {
        }
    }
}