using System;
using UnityEngine;
using Xynok.OOP.Data.Core.Primitives;

namespace Xynok.OOP.Data.Core.Dictionary
{
    [Serializable]
    public class AMappingDataBinding<T0, T> : APrimitiveDataBinding<T>
    {
        [SerializeField] private T0 key;

        public T0 Key => key;

        public AMappingDataBinding(T0 key, T value) : base(value)
        {
            this.key = key;
        }
    }
}