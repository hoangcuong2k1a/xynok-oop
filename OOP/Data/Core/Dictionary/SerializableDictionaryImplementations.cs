using System;
using Xynok.OOP.Data.Core.Dictionary;

namespace Zk1Core.Runtime.Data.Core.Dictionary
{
    // ---------------
//  String => Int
// ---------------
    [Serializable]
    public class StringIntDictionary : SerializableDictionary<string, int> {}

// ---------------
//  String => String
// ---------------
    [Serializable]
    public class StringStringDictionary : SerializableDictionary<string, string> {}
}


