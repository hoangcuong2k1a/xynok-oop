using System;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Dictionary;

namespace Xynok.OOP.Data.Primitives
{
    [Serializable]
    public class AMappingDataBindingForRuntime<T1, T2> : AMappingDataBinding<T1, T2>
    {
        private int _hashKey;

        public int GetHashOfKey()
        {
            if (_hashKey == default) _hashKey = Animator.StringToHash(Key.ToString());
            return _hashKey;
        }

        public AMappingDataBindingForRuntime(T1 key, T2 value) : base(key, value)
        {
        }
    }

    [Serializable]
    public class FloatData : AMappingDataBindingForRuntime<FloatDataType, float>
    {
        public FloatData(FloatDataType key, float value) : base(key, value)
        {
        }
    }

    [Serializable]
    public class BoolData : AMappingDataBindingForRuntime<BoolDataType, bool>
    {
        public BoolData(BoolDataType key, bool value) : base(key, value)
        {
        }
    }

    [Serializable]
    public class Vector2Data : AMappingDataBindingForRuntime<Vector2DataType, Vector2>
    {
        public Vector2Data(Vector2DataType key, Vector2 value) : base(key, value)
        {
        }
    }

    [Serializable]
    public class Vector3Data : AMappingDataBindingForRuntime<Vector3DataType, Vector3>
    {
        public Vector3Data(Vector3DataType key, Vector3 value) : base(key, value)
        {
        }
    }

    [Serializable]
    public class ATriggerData<T> : AMappingDataBindingForRuntime<T, bool> where T : Enum
    {
        public ATriggerData(T key, bool value) : base(key, value)
        {
        }
    }

    [Serializable]
    public class TriggerData : ATriggerData<TriggerType>
    {
        public TriggerData(TriggerType key, bool value) : base(key, value)
        {
        }

        // public override bool Value
        // {
        //     get => currentValue;
        //     set
        //     {
        //         lastValue = currentValue;
        //         currentValue = value;
        //         EmitDeepEventChanged();
        //         EmitEventChanged();
        //         
        //         // reset after trigger
        //         currentValue = false;
        //         lastValue = value;
        //     }
        // }
    }
}