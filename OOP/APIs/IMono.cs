﻿using UnityEngine;

namespace Xynok.OOP.APIs
{
    public interface IMono
    {
        GameObject GameObj { get; }
    }
}