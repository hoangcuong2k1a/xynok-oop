﻿using System;
using ALCCore.Data;
using ALCCore.Enums;
using Xynok.APIs;
using Xynok.Enums;
using Xynok.OOP.Data;
using Xynok.OOP.Data.Core;
using Xynok.OOP.Data.Core.Dictionary;
using Xynok.OOP.Data.Primitives;
using Zk1Core.Data;

namespace Zk1Core.Services.StateConditions.Core
{
    [Serializable]
    public class BoolDataInvoker
    {
        public BoolDataType invoker;
        public bool invokerValue;
        public BoolDataImpactValue impacts;
    }

    [Serializable]
    public class BoolDataImpactValue : SerializableDictionary<BoolDataType, bool>
    {
    }

    /// <summary>
    /// Khi một state thay đổi sẽ ảnh hưởng tới value của các state khác
    /// </summary>
    [Serializable]
    public class BoolDataInvokeImpact : IInjectable<ResourceBase>
    {
        public BoolDataInvoker[] changers;

        private ResourceBase _source;
        private Action _onDispose;

        public void SetDependency(ResourceBase dependency)
        {
            if (_source != null) Dispose();
            _source = dependency;
            Init();
        }

        void Init()
        {
            for (int i = 0; i < changers.Length; i++)
            {
                int index = i;
                BindingChange(changers[index]);
            }
        }

        void BindingChange(BoolDataInvoker invokerData)
        {
            // invokers
            BoolData invoker = _source.GetState(invokerData.invoker);

            // impacts
            BoolData[] impacts = new BoolData[invokerData.impacts.Count];
            int count = 0;
            foreach (var pair in invokerData.impacts)
            {
                impacts[count] = _source.GetState(pair.Key);
            }

            // binding
            void Impact(bool value)
            {
                if (value == invokerData.invokerValue)
                {
                    for (int i = 0; i < impacts.Length; i++)
                        impacts[i].Value = invokerData.impacts[impacts[i].Key];
                }
            }

            invoker.AddListener(Impact);
            _onDispose += () => { invoker.RemoveListener(Impact); };
        }

        public void Dispose()
        {
            _onDispose?.Invoke();
            _onDispose = default;
        }
    }
}