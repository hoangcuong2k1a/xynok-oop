﻿using Zk1Core.Services.StateConditions.Core;

namespace Zk1Core.Services.StateConditions.APIs
{
    public interface IBoolDataInvokeImpact
    {
        BoolDataInvokeImpact GetBoolDataInvokeImpact();
    }
}