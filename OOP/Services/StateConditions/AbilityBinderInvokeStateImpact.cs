﻿using ALCCore.Data;
using ALCCore.Entities.APIs;
using ALCCore.Enums;
using ALCCore.Services.Abilities.Core;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data;
using Xynok.OOP.Data.Core;
using Xynok.OOP.Entities.APIs;
using Xynok.OOP.Services.Abilities.Core;
using Zk1Core.Patterns.Adapter;
using Zk1Core.Services.StateConditions.APIs;

namespace ALCCore.Services.StateConditions
{
    public class AbilityBinderInvokeStateImpact: AAbility
    {
        protected override AbilityType AbilityType => AbilityType.BinderInvokeStateImpact;

        private Adapter _adapter;
        protected override void Init()
        {
            _adapter = new Adapter(Owner);
            _adapter.BoolDataInvokeImpact.GetBoolDataInvokeImpact().SetDependency(_adapter.ResourceBase);
        }

        protected override bool Matched()
        {
            throw new System.NotImplementedException();
        }

        protected override void OnDispose()
        {
            _adapter.BoolDataInvokeImpact.GetBoolDataInvokeImpact().Dispose();
        }
        
        private class Adapter: AAdapter<IAbilityOwner>
        {
            internal ResourceBase ResourceBase;
            internal IBoolDataInvokeImpact BoolDataInvokeImpact;
            public Adapter(IAbilityOwner source) : base(source)
            {
                if (Source is IEntity entity)
                {
                    if (Source is IBoolDataInvokeImpact invokeImpact)
                    {
                        ResourceBase = entity.Data;
                        BoolDataInvokeImpact = invokeImpact;
                        return;
                    }
                }

                Debug.LogError($"{Source.GetType().Name} cannot have ability BinderInvokeStateImpact");
            }
        }
    }
}