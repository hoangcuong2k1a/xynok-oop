﻿using System;
using System.Collections.Generic;
using ALCCore.Entities.APIs;
using ALCCore.Enums;
using ALCCore.Managers;
using UnityEngine;
using Xynok.APIs;
using Xynok.Enums;
using Xynok.OOP.Services.Abilities.Core;

namespace ALCCore.Services.Abilities.Core
{
    /// <summary>
    /// Chịu trách nhiệm khởi tạo ability cho obj khi:
    /// - obj init
    /// - obj remove ability
    /// - obj add ability
    /// </summary>
    public class AbilityController : IInjectable<IAbilityOwner>
    {
        protected IAbilityOwner Owner;
        protected Action OnDispose;
        protected List<IAbility> Abilities;

        public void SetDependency(IAbilityOwner dependency)
        {
            if (Owner != null) Dispose();
            
            Owner = dependency;
            Abilities = new();
            
            Owner.Abilities.OnAdd += InitNewAbility;
            Owner.Abilities.OnRemove += RemoveAbility;
            
            InitDefaultAbilities();
        }

        void InitDefaultAbilities()
        {
            for (int i = 0; i < Owner.Abilities.Count; i++)
                InitNewAbility(Owner.Abilities[i]);
        }

       protected virtual void InitNewAbility(AbilityType abilityType)
        {
            bool duplicatedAbility = Abilities.Find(e => e.GetProductType() == abilityType) != null;

            if (duplicatedAbility)
            {
                Debug.LogError($"owner {Owner.GetType().Name} duplicated ability {abilityType}!");
                return;
            }

            var ability = FactoryOop.Instance.AbilityFactory.GetProduct(abilityType);
            ability.SetDependency(Owner);
            OnDispose += ability.Dispose;
            Abilities.Add(ability);
        }

        void RemoveAbility(AbilityType abilityType)
        {
            var ability = Abilities.Find(e => e.GetProductType() == abilityType);
            bool existed = ability != null;
            if (!existed)
            {
                Debug.LogError($"owner {Owner.GetType().Name} does not contain ability {abilityType} to be removed !");
                return;
            }

            OnDispose -= ability.Dispose;
            ability.Dispose();
            Abilities.Remove(ability);
        }

        public void Dispose()
        {
            Owner.Abilities.OnAdd -= InitNewAbility;
            Owner.Abilities.OnRemove -= RemoveAbility;
            OnDispose?.Invoke();
            OnDispose = default;
            Abilities?.Clear();
        }
    }
}