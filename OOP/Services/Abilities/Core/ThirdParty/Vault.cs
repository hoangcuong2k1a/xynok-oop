using Xynok.Patterns.Singleton;
using Zk1Core.Patterns.Singleton;

namespace Zk1Core.Services.Abilities.Core.ThirdParty
{
    /// <summary>
    /// lưu trữ dữ liệu lung tung của runtime modules. Tiện cho việc điều chỉnh và cân bằng truớc release
    /// </summary>
    public class Vault: ASingleton<Vault>
    {
        public PrimitiveDataVaultDict primitiveDataVaultDict;
    }


}