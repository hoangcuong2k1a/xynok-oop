﻿using System;
using System.Collections.Generic;
using Xynok.OOP.Data.Core.Primitives;
using Zk1Core.Data.APIs;

namespace Zk1Core.Services.Abilities.Core.ThirdParty
{
    [Serializable]
    public class PrimitiveDataVaultDict : Dictionary<string, IPrimitiveBinder>
    {
    }

    public class ADataVault<T> : IDisposable where T : IPrimitiveBinder
    {
        private T _data;
        private string _name;
        public T Data => _data;
        public ADataVault(string name, T data)
        {
            _data = data;
            _name = name;
            Vault.Instance.primitiveDataVaultDict.Add(_name, _data);
        }

        public void Dispose()
        {
            Vault.Instance.primitiveDataVaultDict.Remove(_name);
        }
    }
    public class FloatVault: ADataVault<FloatValue>
    {
        public FloatVault(string name, FloatValue data) : base(name, data)
        {
        }
    }
}