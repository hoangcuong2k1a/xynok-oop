﻿using ALCCore.Entities.APIs;
using Xynok.APIs;
using Xynok.Enums;
using Xynok.Patterns.Factory.Reflection;

namespace Xynok.OOP.Services.Abilities.Core
{
    public interface IAbility : ITypeOnFactory<AbilityType>, IInjectable<IAbilityOwner>
    {
    }

    public abstract class AAbility : IAbility
    {
        protected abstract AbilityType AbilityType { get; }
        protected IAbilityOwner Owner;
        public AbilityType GetProductType() => AbilityType;

        public void SetDependency(IAbilityOwner dependency)
        {
            if (Owner != null) Dispose();
            Owner = dependency;
            if (!Matched()) return;
            Init();
        }

        public void Dispose()
        {
            OnDispose();
        }

        protected abstract bool Matched();
        protected abstract void Init();
        protected abstract void OnDispose();
    }
}