﻿using Xynok.Enums;
using Xynok.OOP.Services.Abilities.Core;
using Xynok.Patterns.Factory.Reflection;

namespace Xynok.Core.Services.Abilities.Core
{
    public class AbilityFactory : AFactory<AbilityType, AAbility>
    {
    }
}