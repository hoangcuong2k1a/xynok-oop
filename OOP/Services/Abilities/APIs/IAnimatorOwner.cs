﻿using UnityEngine;

namespace Xynok.OOP.Services.Abilities.APIs
{
    public interface IAnimatorOwner
    {
        Animator Animator { get; }
    }
}