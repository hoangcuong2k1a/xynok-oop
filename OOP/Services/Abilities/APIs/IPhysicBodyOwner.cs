﻿using System;
using UnityEngine;

namespace Zk1Core.Services.Abilities.APIs
{
    public interface IPhysicBodyOwner
    {
        Rigidbody PhysicBody { get; }
    }

    public interface IPhysicTrigger
    {
        event Action<GameObject> TriggerEnter;
        event Action<GameObject> TriggerExit;
        event Action<GameObject> TriggerStay;
    }

    public interface IPhysicCollision3D
    {
        public event Action<Collision> CollisionStay;
        public event Action<Collision> CollisionEnter;
        public event Action<Collision> CollisionExit;
    }
}