﻿using System;

namespace Zk1Core.Services.Abilities.APIs
{
    public interface IDrawGizmos
    {
        public event Action DrawGizmos;
    }
}