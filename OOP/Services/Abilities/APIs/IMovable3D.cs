﻿using Xynok.OOP.Data.Core.Primitives;

namespace Zk1Core.Services.Abilities.APIs
{
    public interface IMovable3D
    {
        Vector3Value MoveDirection { get; }
    }
}