﻿using UnityEngine;

namespace Zk1Core.Services.Abilities.APIs
{
    public interface IGroundTracking
    {
        Transform GroundPoint { get; }
    }
}