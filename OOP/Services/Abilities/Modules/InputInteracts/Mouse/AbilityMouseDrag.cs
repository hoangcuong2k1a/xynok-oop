﻿using ALCCore.Enums;
using ALCCore.Services.Abilities.Core;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Services.Abilities.Core;
using Zk1Core.Inputs.Mouse.APIs;
using Zk1Core.Inputs.Mouse.Drag;

namespace ALCCore.Services.Abilities.Modules.InputInteracts.Mouse
{
    /// <summary>
    /// Drag object by holding mouse
    /// </summary>
    public class AbilityMouseDrag : AAbility
    {
        private IMouseDraggable _dragger;
        private Draggable _draggableComponent;
        protected override AbilityType AbilityType => AbilityType.MouseDrag;

        protected override void Init()
        {
            if (_dragger != null) Dispose();
            if (!Matching()) return;
            _draggableComponent = _dragger.GameObj.GetComponent<Draggable>();
            _draggableComponent ??= _dragger.GameObj.AddComponent<Draggable>();
            _draggableComponent.SetDependency(_dragger);
        }

        protected override bool Matched()
        {
            throw new System.NotImplementedException();
        }

        bool Matching()
        {
            if (Owner is IMouseDraggable dragger)
            {
                _dragger = dragger;
                return true;
            }

            Debug.LogError("AbilityMouseDrag: Owner is not IMouseDraggable");
            return false;
        }

        protected override void OnDispose()
        {
            if (_draggableComponent)
            {
                _draggableComponent.Dispose();
                Object.Destroy(_draggableComponent);
            }
        }
    }
}