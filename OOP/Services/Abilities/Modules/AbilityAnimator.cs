﻿using System;
using ALCCore.Services.Abilities.Core;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Primitives;
using Xynok.OOP.Entities.APIs;
using Xynok.OOP.Services.Abilities.APIs;
using Xynok.OOP.Services.Abilities.Core;

namespace Xynok.OOP.Services.Abilities.Modules
{
    public interface IAbilityAnimator : IEntity, IAnimatorOwner
    {
    }

    /// <summary>
    /// binding dữ liệu của owner với animator's params
    /// </summary>
    public sealed class AbilityAnimator : AAbility
    {
        private Animator Animator => _abilityAnimator.Animator;
        private FloatData[] Stats => _abilityAnimator.Data.stats;
        private BoolData[] States => _abilityAnimator.Data.states;
        private TriggerData[] Triggers => _abilityAnimator.Data.triggers;
        private IAbilityAnimator _abilityAnimator;

        private Action _onDispose;

        protected override AbilityType AbilityType => AbilityType.Animator;

        protected override void Init()
        {
            InitAnimatorParams();
        }

        protected override bool Matched()
        {
            if (Owner is IAbilityAnimator abilityAnimator)
            {
                _abilityAnimator = abilityAnimator;
                return true;
            }

            Debug.LogError($"{Owner.GetType().Name} can not to be converted to IAbilityAnimator !");
            return false;
        }

        async void InitAnimatorParams()
        {
#if UNITY_EDITOR
            int statStateAmount = Stats.Length + States.Length;

            if (Animator.parameterCount != statStateAmount)
            {
                Debug.LogWarning(
                    $"animator has {Animator.parameterCount} params, but player has {statStateAmount} stats and states");
                await UniTask.WaitUntil(() =>
                    Animator.runtimeAnimatorController != null); // sử dụng khi 1 hero có nhiều vũ khí

                if (Animator.runtimeAnimatorController is AnimatorOverrideController animatorOverrideController)
                {
                    _animatorController =
                        (UnityEditor.Animations.AnimatorController)animatorOverrideController.runtimeAnimatorController;
                }
                else
                {
                    _animatorController = (UnityEditor.Animations.AnimatorController)Animator.runtimeAnimatorController;
                }

                ClearParamOfAnimator();
                InitParamForAnimator();
                await UniTask.Delay(TimeSpan.FromSeconds(2));
            }
#endif
            Binding();
        }

#if UNITY_EDITOR

        private UnityEditor.Animations.AnimatorController _animatorController;

        void InitParamForAnimator()
        {
            for (int i = 0; i < Stats.Length; i++)
                _animatorController.AddParameter(Stats[i].Key.ToString(),
                    AnimatorControllerParameterType.Float);

            for (int i = 0; i < States.Length; i++)
                _animatorController.AddParameter(States[i].Key.ToString(),
                    AnimatorControllerParameterType.Bool);

            for (int i = 0; i < Triggers.Length; i++)
                _animatorController.AddParameter(Triggers[i].Key.ToString(),
                    AnimatorControllerParameterType.Trigger);
        }

        void ClearParamOfAnimator()
        {
            for (int i = _animatorController.parameters.Length - 1; i > -1; i--)
            {
                int index = i;
                _animatorController.RemoveParameter(index);
            }
        }
#endif

        void Binding()
        {
            // stats
            for (int i = 0; i < Stats.Length; i++)
            {
                int index = i;
                FloatData statData = Stats[index];

                void UpdateAnimParam(float value)
                {
                    Animator.SetFloat(statData.GetHashOfKey(), value);
                }

                UpdateAnimParam(statData.Value);

                statData.AddListener(UpdateAnimParam);
                _onDispose += () => { statData.RemoveListener(UpdateAnimParam); };
            }

            // states
            for (int i = 0; i < States.Length; i++)
            {
                int index = i;
                BoolData stateData = States[index];

                void UpdateAnimParam(bool value)
                {
                    Animator.SetBool(stateData.GetHashOfKey(), value);
                }

                UpdateAnimParam(stateData.Value);
                stateData.AddListener(UpdateAnimParam);
                _onDispose += () => { stateData.RemoveListener(UpdateAnimParam); };
            }

            // triggers
            for (int i = 0; i < Triggers.Length; i++)
            {
                int index = i;
                TriggerData data = Triggers[index];

                void UpdateAnimParam(bool value)
                {
                    if (value) Animator.SetTrigger(data.GetHashOfKey());
                }

                UpdateAnimParam(data.Value);
                data.AddListener(UpdateAnimParam);
                _onDispose += () => { data.RemoveListener(UpdateAnimParam); };
            }
        }


        protected override void OnDispose()
        {
            _onDispose?.Invoke();
            _onDispose = default;
        }
    }
}