﻿using ALCCore.Entities.APIs;
using ALCCore.Enums;
using ALCCore.Services.Abilities.Core;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Primitives;
using Xynok.OOP.Data.Primitives;
using Xynok.OOP.Entities.APIs;
using Xynok.OOP.Services.Abilities.Core;
using Zk1Core.Patterns.Adapter;
using Zk1Core.Services.Abilities.APIs;

namespace ALCCore.Services.Abilities.Modules.StateTracking
{
    /// <summary>
    /// update movement states of ability owner when he's moving
    /// </summary>
    public class AbilityMovementTrackingDirection : AAbility
    {
        protected override AbilityType AbilityType => AbilityType.MovementTrackingDirection;
        private Adapter _adapter;

        protected override void Init()
        {
            _adapter = new(Owner);
            _adapter.MoveDirection.AddListener(OnMoveDirectionChanged);
        }

        protected override bool Matched()
        {
            throw new System.NotImplementedException();
        }

        void OnMoveDirectionChanged(Vector3 direction)
        {
            _adapter.Moving.Value = direction.x != 0 || direction.z != 0;
        }

        protected override void OnDispose()
        {
            _adapter.MoveDirection.RemoveListener(OnMoveDirectionChanged);
        }

        private class Adapter : AAdapter<IAbilityOwner>
        {
            internal BoolData Moving;
            internal Vector3Value MoveDirection;

            public Adapter(IAbilityOwner source) : base(source)
            {
                if (Source is IEntity entity)
                {
                    if (Source is IMovable3D mover)
                    {
                        MoveDirection = mover.MoveDirection;
                        Moving = entity.Data.GetState(BoolDataType.Moving);
                    }

                    return;
                }

                Debug.LogError($"{Source.GetType().Name} does not have movement data !");
            }
        }
    }
}