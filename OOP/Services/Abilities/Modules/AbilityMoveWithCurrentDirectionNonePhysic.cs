﻿using ALCCore.Entities.APIs;
using ALCCore.Enums;
using ALCCore.Services.Abilities.Core;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Primitives;
using Xynok.OOP.Data.Primitives;
using Xynok.OOP.Entities.APIs;
using Xynok.OOP.Services.Abilities.Core;
using Xynok.Utils;
using Zk1Core.Data.Enums;
using Zk1Core.Entities.APIs;
using Zk1Core.Patterns.Adapter;
using Zk1Core.Services.Abilities.APIs;
using Zk1Core.Utils;

namespace ALCCore.Services.Abilities.Modules
{
    public class AbilityMoveWithCurrentDirectionNonePhysic : AAbility
    {
        protected override AbilityType AbilityType => AbilityType.MoveWithCurrentDirectionNonePhysic;
        private Adapter _adapter;

        protected override void Init()
        {
            _adapter = new(Owner);
            _adapter.MoveReady.Value = true;
            TimeCycle.Instance.AddListenerOnUpdate(HoldSpace);
        }

        protected override bool Matched()
        {
            throw new System.NotImplementedException();
        }

        void HoldSpace()
        {
            if (Input.GetKey(KeyCode.Space))
            {
                if (!_adapter.MoveReady.Value) return;


                // move ...
                Vector3 moveDirection = _adapter.GlobalTrans.rotation * Vector3.forward;
                _adapter.GlobalTrans.Translate(moveDirection * _adapter.MoveSpeed.Value * Time.deltaTime,
                    Space.World);
                _adapter.MoveDirection.Value = moveDirection;
            }
            else
            {
                if (_adapter.MoveDirection.Value != Vector3.zero) _adapter.MoveDirection.Value = Vector3.zero;
            }
        }

        protected override void OnDispose()
        {
            if (TimeCycle.Instance) TimeCycle.Instance.RemoveListenerOnUpdate(HoldSpace);
        }

        private class Adapter : AAdapter<IAbilityOwner>
        {
            internal Transform GlobalTrans;
            internal FloatData MoveSpeed;
            internal BoolData MoveReady;
            internal Vector3Value MoveDirection;


            public Adapter(IAbilityOwner source) : base(source)
            {
                if (Source is IEntity entity)
                {
                    MoveSpeed = entity.Data.GetStat(FloatDataType.MoveSpeed);
                    MoveReady = entity.Data.GetState(BoolDataType.MoveReady);
                }

                if (Source is IPosition monoObj)
                {
                    GlobalTrans = monoObj.GlobalTransform;
                }

                if (Source is IMovable3D mover)
                {
                    MoveDirection = mover.MoveDirection;
                    return;
                }

                Debug.LogError($"{Source.GetType().Name} can not move because it's not an entity !");
            }
        }
    }
}