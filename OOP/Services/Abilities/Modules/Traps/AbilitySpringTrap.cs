﻿using ALCCore.Entities.APIs;
using ALCCore.Enums;
using ALCCore.Services.Abilities.Core;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Data.Core.Primitives;
using Xynok.OOP.Data.Primitives;
using Xynok.OOP.Entities.APIs;
using Xynok.OOP.Services.Abilities.Core;
using Zk1Core.Patterns.Adapter;
using Zk1Core.Services.Abilities.APIs;
using Zk1Core.Services.Abilities.Core.ThirdParty;

namespace ALCCore.Services.Abilities.Modules.Traps
{
    public class AbilitySpringTrap : AAbility
    {
        protected override AbilityType AbilityType => AbilityType.TrapSpring;
        private Adapter _adapter;
        private string _name = "Spring Trap Force";
        private FloatVault _springForce;

        protected override void Init()
        {
            _adapter = new(Owner);

            InitVault();

            _adapter.PhysicTrigger.TriggerEnter += SpringUp;
        }

        protected override bool Matched()
        {
            throw new System.NotImplementedException();
        }

        void InitVault()
        {
            _name = $" [{_adapter.GlobalTrans.gameObject.name}] Spring Trap Force";
            var data = new FloatValue(6f);
            // data.SetBaseValue(6f);
            _springForce = new(_name, data);
        }

        void SpringUp(GameObject obj)
        {
            var physicBodyOwner = obj.GetComponent<IPhysicBodyOwner>();

            if (physicBodyOwner == null)
            {
                Debug.LogError($"{obj.name} does not implement IPhysicBodyOwner ! ");
                return;
            }


            physicBodyOwner.PhysicBody.AddForce(Vector3.up * _springForce.Data.Value, ForceMode.Impulse);
            physicBodyOwner.PhysicBody.AddForce(_adapter.GlobalTrans.forward * (_springForce.Data.Value / 2),
                ForceMode.Impulse);

            //   HandleSpringTarget(obj);
        }

        void HandleSpringTarget(GameObject obj)
        {
            IEntity entity = obj.GetComponent<IEntity>();
            if (entity == null) return;
            BoolData onTheGround = entity.Data.GetState(BoolDataType.TheGround);
            onTheGround.Value = false;
        }

        protected override void OnDispose()
        {
            _springForce.Dispose();
            _adapter.PhysicTrigger.TriggerEnter -= SpringUp;
        }

        private class Adapter : AAdapter<IAbilityOwner>
        {
            internal IPhysicTrigger PhysicTrigger;
            internal Transform GlobalTrans;

            public Adapter(IAbilityOwner source) : base(source)
            {
                if (Source is IEntity entity)
                {
                    GlobalTrans = entity.GlobalTransform;
                }

                if (Source is IPhysicTrigger physicTrigger)
                {
                    PhysicTrigger = physicTrigger;
                    return;
                }


                Debug.LogError(
                    $"{Source.GetType().Name} cannot has ability TrapSpring because it does have rigidbody !");
            }
        }
    }
}