﻿using ALCCore.Entities.APIs;
using ALCCore.Enums;
using ALCCore.Services.Abilities.Core;
using UnityEngine;
using Xynok.Enums;
using Xynok.OOP.Entities.APIs;
using Xynok.OOP.Services.Abilities.Core;
using Zk1Core.Patterns.Adapter;
using Zk1Core.Services.Abilities.APIs;

namespace ALCCore.Services.Abilities.GizmosRunner
{
    /// <summary>
    /// visualize 3 hướng x,y,z của obj
    /// </summary>
    public class AbilityGizmosDrawTransformDirection : AAbility
    {
        protected override AbilityType AbilityType => AbilityType.GizmosDrawTransformDirection;
        private Adapter _adapter;
        private float _length = 2;

        private class Adapter : AAdapter<IAbilityOwner>
        {
            internal IDrawGizmos Gizmos;
            internal IEntity Entity;

            public Adapter(IAbilityOwner source) : base(source)
            {
                if (Source is IDrawGizmos drawGizmos)
                {
                    if (Source is IEntity entity)
                    {
                        Gizmos = drawGizmos;
                        Entity = entity;
                        return;
                    }

                    Debug.LogError($"{Source.GetType().Name} can not has ability GizmosDrawTransformDirection ! ");
                }
            }
        }

        protected override void Init()
        {
            _adapter = new Adapter(Owner);
            _adapter.Gizmos.DrawGizmos += Draw;
        }

        protected override bool Matched()
        {
            return false;
        }

        void Draw()
        {
            Vector3 currentPos = _adapter.Entity.CurrentPosition;

            Gizmos.color = Color.blue;
            Gizmos.DrawRay(currentPos, _adapter.Entity.GlobalTransform.forward);
            Gizmos.color = Color.green;
            Gizmos.DrawRay(currentPos, _adapter.Entity.GlobalTransform.up);
            Gizmos.color = Color.red;
            Gizmos.DrawRay(currentPos, _adapter.Entity.GlobalTransform.right);
        }

        protected override void OnDispose()
        {
            _adapter.Gizmos.DrawGizmos -= Draw;
        }
    }
}