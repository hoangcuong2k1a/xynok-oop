using System;

namespace UnityScreenNavigator.Runtime.Core.Page
{
    public sealed class AnonymousPageContainerCallbackReceiver : IPageContainerCallbackReceiver
    {
        public AnonymousPageContainerCallbackReceiver(
            Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> onBeforePush = null,
            Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> onAfterPush = null,
            Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> onBeforePop = null,
            Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> onAfterPop = null)
        {
            OnBeforePush = onBeforePush;
            OnAfterPush = onAfterPush;
            OnBeforePop = onBeforePop;
            OnAfterPop = onAfterPop;
        }

        void IPageContainerCallbackReceiver.BeforePush(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)
        {
            OnBeforePush?.Invoke((enterPage, exitPage));
        }

        void IPageContainerCallbackReceiver.AfterPush(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)
        {
            OnAfterPush?.Invoke((enterPage, exitPage));
        }

        void IPageContainerCallbackReceiver.BeforePop(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)
        {
            OnBeforePop?.Invoke((enterPage, exitPage));
        }

        void IPageContainerCallbackReceiver.AfterPop(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)
        {
            OnAfterPop?.Invoke((enterPage, exitPage));
        }

        public event Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> OnAfterPop;
        public event Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> OnAfterPush;
        public event Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> OnBeforePop;
        public event Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> OnBeforePush;
    }
}
