using System;
using Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page;
using UnityScreenNavigator.Runtime.Core.Shared;

namespace UnityScreenNavigator.Runtime.Core.Page
{
    public static class PageContainerExtensions
    {
        /// <summary>
        ///     Add callbacks.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="onBeforePush"></param>
        /// <param name="onAfterPush"></param>
        /// <param name="onBeforePop"></param>
        /// <param name="onAfterPop"></param>
        public static void AddCallbackReceiver(this PageContainer self,
            Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> onBeforePush = null,
            Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> onAfterPush = null,
            Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> onBeforePop = null,
            Action<(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage)> onAfterPop = null)
        {
            var callbackReceiver =
                new AnonymousPageContainerCallbackReceiver(onBeforePush, onAfterPush, onBeforePop, onAfterPop);
            self.AddCallbackReceiver(callbackReceiver);
        }

        /// <summary>
        ///     Add callbacks.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="page"></param>
        /// <param name="onBeforePush"></param>
        /// <param name="onAfterPush"></param>
        /// <param name="onBeforePop"></param>
        /// <param name="onAfterPop"></param>
        public static void AddCallbackReceiver(this PageContainer self, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page page,
            Action<global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page> onBeforePush = null, Action<global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page> onAfterPush = null,
            Action<global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page> onBeforePop = null, Action<global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page> onAfterPop = null)
        {
            var callbackReceiver = new AnonymousPageContainerCallbackReceiver();
            callbackReceiver.OnBeforePush += x =>
            {
                var (enterPage, exitPage) = x;
                if (enterPage.Equals(page))
                {
                    onBeforePush?.Invoke(exitPage);
                }
            };
            callbackReceiver.OnAfterPush += x =>
            {
                var (enterPage, exitPage) = x;
                if (enterPage.Equals(page))
                {
                    onAfterPush?.Invoke(exitPage);
                }
            };
            callbackReceiver.OnBeforePop += x =>
            {
                var (enterPage, exitPage) = x;
                if (exitPage.Equals(page))
                {
                    onBeforePop?.Invoke(enterPage);
                }
            };
            callbackReceiver.OnAfterPop += x =>
            {
                var (enterPage, exitPage) = x;
                if (exitPage.Equals(page))
                {
                    onAfterPop?.Invoke(enterPage);
                }
            };

            var gameObj = self.gameObject;
            if (!gameObj.TryGetComponent<MonoBehaviourDestroyedEventDispatcher>(out var destroyedEventDispatcher))
            {
                destroyedEventDispatcher = gameObj.AddComponent<MonoBehaviourDestroyedEventDispatcher>();
            }

            destroyedEventDispatcher.OnDispatch += () => self.RemoveCallbackReceiver(callbackReceiver);

            self.AddCallbackReceiver(callbackReceiver);
        }
    }
}