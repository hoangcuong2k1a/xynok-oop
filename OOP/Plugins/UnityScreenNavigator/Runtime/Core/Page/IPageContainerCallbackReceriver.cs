namespace UnityScreenNavigator.Runtime.Core.Page
{
    public interface IPageContainerCallbackReceiver
    {
        void BeforePush(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage);

        void AfterPush(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage);

        void BeforePop(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage);

        void AfterPop(global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page enterPage, global::Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Page.Page exitPage);
    }
}