namespace UnityScreenNavigator.Runtime.Core.Sheet
{
    public interface ISheetContainerCallbackReceiver
    {
        void BeforeShow(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet);

        void AfterShow(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet);

        void BeforeHide(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet);

        void AfterHide(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet);
    }
}