using System;

namespace UnityScreenNavigator.Runtime.Core.Sheet
{
    public static class SheetContainerExtensions
    {
        /// <summary>
        ///     Add callbacks.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="onBeforeShow"></param>
        /// <param name="onAfterShow"></param>
        /// <param name="onBeforeHide"></param>
        /// <param name="onAfterHide"></param>
        public static void AddCallbackReceiver(this SheetContainer self,
            Action<(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)> onBeforeShow = null,
            Action<(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)> onAfterShow = null,
            Action<VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet> onBeforeHide = null, Action<VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet> onAfterHide = null)
        {
            var callbackReceiver =
                new AnonymousSheetContainerCallbackReceiver(onBeforeShow, onAfterShow, onBeforeHide, onAfterHide);
            self.AddCallbackReceiver(callbackReceiver);
        }
    }
}