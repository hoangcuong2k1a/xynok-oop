using System;

namespace UnityScreenNavigator.Runtime.Core.Sheet
{
    public sealed class AnonymousSheetContainerCallbackReceiver : ISheetContainerCallbackReceiver
    {
        public AnonymousSheetContainerCallbackReceiver(
            Action<(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)> onBeforeShow = null,
            Action<(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)> onAfterShow = null,
            Action<VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet> onBeforeHide = null, Action<VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet> onAfterHide = null)
        {
            OnBeforeShow = onBeforeShow;
            OnAfterShow = onAfterShow;
            OnBeforeHide = onBeforeHide;
            OnAfterHide = onAfterHide;
        }

        public void BeforeShow(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)
        {
            OnBeforeShow?.Invoke((enterSheet, exitSheet));
        }

        public void AfterShow(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)
        {
            OnAfterShow?.Invoke((enterSheet, exitSheet));
        }

        public void BeforeHide(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)
        {
            OnBeforeHide?.Invoke(exitSheet);
        }

        public void AfterHide(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)
        {
            OnAfterHide?.Invoke(exitSheet);
        }

        public event Action<(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)> OnBeforeShow;
        public event Action<(VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet enterSheet, VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet exitSheet)> OnAfterShow;
        public event Action<VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet> OnBeforeHide;
        public event Action<VeThanCore.Plugins.UnityScreenNavigator.Runtime.Core.Sheet.Sheet> OnAfterHide;
    }
}