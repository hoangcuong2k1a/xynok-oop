using System;

namespace UnityScreenNavigator.Runtime.Core.Modal
{
    public sealed class AnonymousModalContainerCallbackReceiver : IModalContainerCallbackReceiver
    {
        public AnonymousModalContainerCallbackReceiver(
            Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> onBeforePush = null,
            Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> onAfterPush = null,
            Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> onBeforePop = null,
            Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> onAfterPop = null)
        {
            OnBeforePush = onBeforePush;
            OnAfterPush = onAfterPush;
            OnBeforePop = onBeforePop;
            OnAfterPop = onAfterPop;
        }

        public void BeforePush(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)
        {
            OnBeforePush?.Invoke((enterModal, exitModal));
        }

        public void AfterPush(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)
        {
            OnAfterPush?.Invoke((enterModal, exitModal));
        }

        public void BeforePop(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)
        {
            OnBeforePop?.Invoke((enterModal, exitModal));
        }

        public void AfterPop(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)
        {
            OnAfterPop?.Invoke((enterModal, exitModal));
        }

        public event Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> OnAfterPop;
        public event Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> OnAfterPush;
        public event Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> OnBeforePop;
        public event Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> OnBeforePush;
    }
}