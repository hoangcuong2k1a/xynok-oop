using System;
using Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal;
using UnityScreenNavigator.Runtime.Core.Shared;
using UnityScreenNavigator.Runtime.Foundation.Coroutine;
using VeThanCore.Plugins.UnityScreenNavigator.Runtime.Foundation.Coroutine;
using Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal;

namespace UnityScreenNavigator.Runtime.Core.Modal
{
    public static class ModalContainerExtensions
    {
        /// <summary>
        ///     Add callbacks.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="onBeforePush"></param>
        /// <param name="onAfterPush"></param>
        /// <param name="onBeforePop"></param>
        /// <param name="onAfterPop"></param>
        public static void AddCallbackReceiver(this ModalContainer self,
            Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal,
                Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> onBeforePush = null,
            Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal,
                Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> onAfterPush = null,
            Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal,
                Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> onBeforePop = null,
            Action<(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal,
                Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal)> onAfterPop = null)
        {
            var callbackReceiver =
                new AnonymousModalContainerCallbackReceiver(onBeforePush, onAfterPush, onBeforePop, onAfterPop);
            self.AddCallbackReceiver(callbackReceiver);
        }

        /// <summary>
        ///     Add callbacks.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="modal"></param>
        /// <param name="onBeforePush"></param>
        /// <param name="onAfterPush"></param>
        /// <param name="onBeforePop"></param>
        /// <param name="onAfterPop"></param>
        public static void AddCallbackReceiver(this ModalContainer self,
            Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal modal,
            Action<Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal> onBeforePush = null,
            Action<Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal> onAfterPush = null,
            Action<Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal> onBeforePop = null,
            Action<Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal> onAfterPop = null)
        {
            var callbackReceiver = new AnonymousModalContainerCallbackReceiver();
            callbackReceiver.OnBeforePush += x =>
            {
                var (enterModal, exitModal) = x;
                if (enterModal.Equals(modal))
                {
                    onBeforePush?.Invoke(exitModal);
                }
            };
            callbackReceiver.OnAfterPush += x =>
            {
                var (enterModal, exitModal) = x;
                if (enterModal.Equals(modal))
                {
                    onAfterPush?.Invoke(exitModal);
                }
            };
            callbackReceiver.OnBeforePop += x =>
            {
                var (enterModal, exitModal) = x;
                if (exitModal.Equals(modal))
                {
                    onBeforePop?.Invoke(enterModal);
                }
            };
            callbackReceiver.OnAfterPop += x =>
            {
                var (enterModal, exitModal) = x;
                if (exitModal.Equals(modal))
                {
                    onAfterPop?.Invoke(enterModal);
                }
            };

            var gameObj = self.gameObject;
            if (!gameObj.TryGetComponent<MonoBehaviourDestroyedEventDispatcher>(out var destroyedEventDispatcher))
            {
                destroyedEventDispatcher = gameObj.AddComponent<MonoBehaviourDestroyedEventDispatcher>();
            }

            destroyedEventDispatcher.OnDispatch += () => self.RemoveCallbackReceiver(callbackReceiver);

            self.AddCallbackReceiver(callbackReceiver);
        }

        
    }
}