namespace UnityScreenNavigator.Runtime.Core.Modal
{
    public interface IModalContainerCallbackReceiver
    {
        void BeforePush(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal);

        void AfterPush(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal);

        void BeforePop(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal);

        void AfterPop(Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal enterModal, Zk1Core.Runtime.Plugins.UnityScreenNavigator.Runtime.Core.Modal.Modal exitModal);
    }
}