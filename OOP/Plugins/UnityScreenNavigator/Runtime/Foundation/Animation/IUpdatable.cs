﻿namespace VeThanCore.Plugins.UnityScreenNavigator.Runtime.Foundation.Animation
{
    internal interface IUpdatable
    {
        void Update(float deltaTime);
    }
}