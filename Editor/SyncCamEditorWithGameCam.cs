﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace ALCCore.Editor
{
    public class SyncCamEditorWithGameCam
    {
        [MenuItem("ALCCore/Editor/Sync Editor Cam With Game Cam", false, 1)]
        public static void SyncEditorCamWithGameCam()
        {
            SceneView sceneCam = SceneView.lastActiveSceneView;
            sceneCam.camera.transform.position = Camera.main.transform.position;
            sceneCam.camera.transform.rotation = Camera.main.transform.rotation;
        }
    }
}
#endif
